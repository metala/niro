package fusefs

import (
	"bytes"
	"context"
	"errors"
	"io"
	iofs "io/fs"

	"log"
	"path/filepath"
	"strings"
	"syscall"

	"github.com/hanwen/go-fuse/v2/fs"
	"github.com/hanwen/go-fuse/v2/fuse"
	"go.metala.org/niro"
	"go.metala.org/niro/tnk"
)

// NewArchiveTree creates the tree of a Niro archive as a FUSE
// InodeEmbedder. The inode can either be mounted as the root of a
// FUSE mount, or added as a child to some other FUSE tree.
func NewArchiveTree(niro *niro.Niro, hash tnk.Hash) (fs.InodeEmbedder, error) {
	archive, err := niro.ReadArchive(hash)
	if err != nil {
		return nil, err
	}

	return &archiveRoot{
		niro:    niro,
		archive: archive,
	}, nil
}

const blockSize = 16384

type archiveRoot struct {
	fs.Inode

	niro    *niro.Niro
	archive *tnk.Archive

	blocks uint64
	inodes uint64
}

// archiveRoot implements NodeOnAdder
var _ = (fs.NodeOnAdder)((*archiveRoot)(nil))
var _ = (fs.NodeStatfser)((*archiveRoot)(nil))

func (r *archiveRoot) OnAdd(ctx context.Context) {
	archive := r.archive
	if r.niro.Verbosity >= 1 {
		log.Printf("Tree Hash: %x", archive.Hash)
		log.Printf("Tree Composite: %t", archive.HashFlags.IsSet(tnk.FlagComposite))
	}
	trd, err := r.niro.ReadFile(archive.Hash, archive.HashFlags.IsSet(tnk.FlagComposite))
	if err != nil {
		log.Fatalf("failed to read fs tree: %s", err)
		return
	}
	defer trd.Close()
	magic, err := tnk.ReadMagic(trd)
	if err != nil {
		log.Fatalf("failed to read tree: %s", err)
		return
	}
	if isTree := bytes.Equal(magic, tnk.TreeMagic); !isTree {
		log.Fatalf("not a tree entry: %#v", magic)
		return
	}

	totalSize := uint64(0)
	r.inodes = 0
	for {
		hdr := tnk.TreeEntry{}
		if err := hdr.Unmarshal(trd); errors.Is(err, io.EOF) {
			break
		} else if err != nil {
			log.Fatalf("failed to read tree entry: %s", err)
			return
		}

		dir, base := hdr.Prefix, filepath.Clean(hdr.FileName)
		p := r.EmbeddedInode()
		for _, comp := range strings.Split(dir, "/") {
			if len(comp) == 0 {
				continue
			}
			ch := p.GetChild(comp)
			if ch == nil {
				ch = p.NewPersistentInode(ctx,
					&memDir{},
					fs.StableAttr{Mode: syscall.S_IFDIR})
				p.AddChild(comp, ch, false)
			}
			p = ch
		}

		var attr fuse.Attr
		headerToFileInfo(&attr, &hdr)
		switch {
		case hdr.Mode&iofs.ModeSymlink > 0:
			_, linkName, err := r.niro.ReadAllObject(hdr.Hash[:])
			if err != nil {
				log.Fatalf("failed to read symlink '%s': %s", hdr.FileName, err)
				return
			}
			l := &fs.MemSymlink{
				Data: linkName,
				Attr: attr,
			}
			p.AddChild(base, r.NewPersistentInode(ctx, l, fs.StableAttr{Mode: syscall.S_IFLNK}), false)
		case hdr.IsDir() || bytes.Equal(hdr.Hash[:], tnk.ZeroHash[:]):
			rf := &memDir{}
			rf.Attr = attr
			p.AddChild(base, r.NewPersistentInode(ctx, rf, fs.StableAttr{Mode: syscall.S_IFDIR}), false)
		default:
			df := &niroRegularFile{
				name:      hdr.FileName,
				niro:      r.niro,
				Composite: hdr.HashFlags.IsSet(tnk.FlagComposite),
				Hash:      hdr.Hash[:],
				Attr:      attr,
			}
			p.AddChild(base, r.NewPersistentInode(ctx, df, fs.StableAttr{}), false)
			totalSize += hdr.Size
		}
		r.inodes += 1
	}
	r.blocks = totalSize / blockSize
}

func (r *archiveRoot) Statfs(ctx context.Context, out *fuse.StatfsOut) syscall.Errno {
	*out = fuse.StatfsOut{
		Bsize:  blockSize,
		Blocks: r.blocks,
		Bfree:  0,
		Bavail: 0,
		Files:  r.inodes,
		Ffree:  0,
	}
	return fs.OK
}

// headerToFileInfo fills a fuse.Attr struct from a tar.Header.
func headerToFileInfo(out *fuse.Attr, h *tnk.TreeEntry) {
	out.Mode = uint32(h.Mode)
	out.Size = uint64(h.Size)
	out.Uid = uint32(h.Uid)
	out.Gid = uint32(h.Gid)
	out.SetTimes(&h.ModTime, &h.ModTime, &h.ModTime)
}
