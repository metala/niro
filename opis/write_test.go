package opis

import (
	"bytes"
	"errors"
	"strings"
	"testing"
	"time"

	"github.com/matryer/is"
)

func TestWriteMany(t *testing.T) {
	buf := bytes.NewBuffer(nil)
	err := WriteMany(buf,
		int16(-16), int32(-32), int64(-64),
		uint16(16), uint32(32), uint64(64),
		"string",
		[]byte{1, 2, 3, 4, 5, 6, 7, 8},

		time.Date(1970, 1, 1, 1, 0, 0, 0, time.UTC),

		[32]byte{
			0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
			0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f,
		},
	)

	is := is.New(t)
	is.NoErr(err)
	result := buf.Bytes()
	is.Equal(result, []byte{
		0xff, 0xf0, // int16(-16)
		0xff, 0xff, 0xff, 0xe0, // int32(-32)
		0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xc0, // int64(-64)

		0x00, 0x10, // uint16(16)
		0x00, 0x00, 0x00, 0x20, // uint32(32)
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, // uint64(64)

		0x00, 6, 's', 't', 'r', 'i', 'n', 'g', // "string"
		0x00, 8, 1, 2, 3, 4, 5, 6, 7, 8, // []byte{1, 2, 3, 4, 5, 6, 7, 8}

		0x00, 0x00, 0x00, 0x00, 0x00, 0x36, 0xee, 0x80, // time.Date(1970, 1, 1, 1, 0, 0, 0, nil) == int64(3600_000)

		// [32]byte
		0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
		0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f,
	})
}

func TestWriteNilBytes(t *testing.T) {
	buf := bytes.NewBuffer(nil)
	err := WriteBytes(buf, nil)

	is := is.New(t)
	is.NoErr(err)
	result := buf.Bytes()
	is.Equal(result, []byte{0x00, 0x00})
}

func TestWriteWrongType(t *testing.T) {
	buf := bytes.NewBuffer(nil)
	err := Write(buf, int(1))

	is := is.New(t)
	is.True(err != nil)
	is.True(strings.HasPrefix(err.Error(), "unsupported type"))
}

func TestWriteMultiStringFailLen(t *testing.T) {
	expectedErr := errors.New("error writing to device")
	r := errWriter{err: expectedErr}
	var s string
	err := WriteMany(&r, s)

	is := is.New(t)
	is.True(err != nil)
	is.Equal(err.Error(), "write arg #0 error: write len error: error writing to device")
}

func TestWriteMultiStringFailBytes(t *testing.T) {
	expectedErr := errors.New("error writing to device")
	r := errWriter{err: expectedErr, discard: 2}
	s := "test"
	err := WriteMany(&r, s)

	is := is.New(t)
	is.True(err != nil)
	is.Equal(err.Error(), "write arg #0 error: write bytes error: error writing to device")
}

func TestWrite32BytesFail(t *testing.T) {
	expectedErr := errors.New("error writing to device")
	r := errWriter{err: expectedErr, discard: 0}
	b := [32]byte{0x00}
	err := Write32Bytes(&r, b)

	is := is.New(t)
	is.True(err != nil)
	is.Equal(err.Error(), "write 32 bytes error: error writing to device")
}
