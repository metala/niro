package niro

import (
	"bytes"
	"encoding/hex"
	"fmt"
	"io"
	"io/fs"

	"github.com/spf13/afero"
	"go.metala.org/niro/tnk"
)

type ArchiveInfo struct {
	Hash    tnk.Hash
	Archive *tnk.Archive
}

func (n *Niro) ListArchives() (chan *ArchiveInfo, chan error) {
	ch := make(chan *ArchiveInfo)
	errc := make(chan error, 1)

	go func() {
		defer close(errc)
		defer close(ch)

		if n.secretKey == nil {
			errc <- errInvalidSecretKey
			return
		}

		objectsFs := afero.NewBasePathFs(n.repoFs, "objects/")
		walkFn := func(path string, info fs.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if info.Mode()&fs.ModeType != 0 {
				// skip non-leafs
				return nil
			}
			objectHex := make([]byte, len(path)-1)
			copy(objectHex[:], path[0:2])
			copy(objectHex[2:], path[3:])
			hash, err := hex.DecodeString(string(objectHex))
			if err != nil {
				return fmt.Errorf("unexpected file: %s", path)
			}

			_, r, err := n.tnk.Open(tnk.Hash(hash), n.secretKey)
			if err != nil {
				return fmt.Errorf("failed to read file: %s", path)
			}
			defer r.Close()

			data := make([]byte, 4)
			if _, err := io.ReadFull(r, data); err != nil {
				return err
			}
			if !bytes.Equal(data, tnk.ArchiveMagic) {
				return nil
			}
			if data, err = io.ReadAll(r); err != nil {
				return err
			}
			ar := tnk.Archive{}
			if err := ar.UnmarshalBytes(data); err != nil {
				return fmt.Errorf("failed to unmarshal archive: %w", err)
			}

			ch <- &ArchiveInfo{
				Hash:    hash,
				Archive: &ar,
			}
			return nil
		}
		if err := afero.Walk(objectsFs, "", walkFn); err != nil {
			errc <- err
			return
		}
	}()

	return ch, errc
}
