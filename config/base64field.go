package config

import (
	"encoding/base64"
)

type base64Field []byte

func (f base64Field) MarshalText() ([]byte, error) {
	return []byte(base64.StdEncoding.EncodeToString(f)), nil
}

func (f *base64Field) UnmarshalText(b []byte) error {
	var err error
	*f, err = base64.StdEncoding.DecodeString(string(b))
	return err
}
