package concurrency

import (
	"context"
	"sync"
)

// see https://www.wwt.com/article/fan-out-fan-in-while-maintaining-order

type ProcessorFunc[InT, OutT any] func(InT) OutT

type workerGroup[InT, OutT any] struct {
	workerC       chan *worker[InT, OutT]
	workerOutputC chan chan OutT
	outputC       chan OutT
	group         *sync.WaitGroup
	stop          func()
}

func NewWorkerGroup[InT, OutT any](ctx context.Context, processor ProcessorFunc[InT, OutT], size int) workerGroup[InT, OutT] {
	ctx, cancel := context.WithCancel(ctx)
	workerGroup := workerGroup[InT, OutT]{
		workerC:       make(chan *worker[InT, OutT], size),
		workerOutputC: make(chan chan OutT, size),
		outputC:       make(chan OutT),
		stop:          cancel,
		group:         &sync.WaitGroup{},
	}
	workerGroup.group.Add(size)
	for i := 0; i < size; i++ {
		w := newWorker[InT, OutT](1)
		w.start(ctx, processor, workerGroup.workerC, workerGroup.group)
	}
	go workerGroup.startOutput(ctx)
	go workerGroup.cleanUp(ctx)
	return workerGroup
}

func (wg *workerGroup[InT, OutT]) startOutput(ctx context.Context) {
	defer close(wg.outputC)
	for woc := range wg.workerOutputC {
		select {
		case wg.outputC <- <-woc:
		case <-ctx.Done():
			return
		}
	}
}

func (wg *workerGroup[InT, OutT]) cleanUp(ctx context.Context) {
	<-ctx.Done()
	wg.group.Wait()
	close(wg.workerOutputC)
	close(wg.workerC)
}

func (wg *workerGroup[InT, OutT]) AddWork(input InT) {
	for w := range wg.workerC {
		w.inputC <- input
		wg.workerOutputC <- w.outputC
		break
	}
}

func (wg *workerGroup[InT, OutT]) Output() chan OutT {
	return wg.outputC
}

func (wg *workerGroup[InT, OutT]) Stop() {
	wg.stop()
}

type worker[InT, OutT any] struct {
	inputC  chan InT
	outputC chan OutT
}

func newWorker[InT, OutT any](bufferSize int) *worker[InT, OutT] {
	return &worker[InT, OutT]{
		inputC:  make(chan InT, bufferSize),
		outputC: make(chan OutT, bufferSize),
	}
}
func (w *worker[InT, OutT]) start(ctx context.Context, processor ProcessorFunc[InT, OutT], registerC chan *worker[InT, OutT], wg *sync.WaitGroup) {
	go func() {
		defer func() {
			close(w.inputC)
			close(w.outputC)
			wg.Done()
		}()
		for {
			registerC <- w
			select {
			case input := <-w.inputC:
				select {
				case w.outputC <- processor(input):
				case <-ctx.Done():
					return
				}
			case <-ctx.Done():
				return
			}
		}
	}()
}
