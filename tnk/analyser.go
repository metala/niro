package tnk

import (
	"hash"
	"io"
	"math"
)

type Analyser struct {
	ckm hash.Hash
	fp  hash.Hash

	// compressability
	comp compressability
}

type HashMaker func() hash.Hash

func NewAnalyser(newCkm, newFp HashMaker, salt []byte) *Analyser {
	ckm := newCkm()
	ckm.Write(salt)
	fp := newFp()

	return &Analyser{
		ckm: ckm,
		fp:  fp,
	}
}

func (a *Analyser) Reset() {
	a.ckm.Reset()
	a.fp.Reset()
	a.comp = compressability{}
}

func (a *Analyser) Write(p []byte) (n int, err error) {
	sz := len(p)
	if n, err = a.ckm.Write(p); err != nil {
		return n, err
	} else if n != sz {
		return n, io.ErrShortWrite
	}
	if n, err = a.fp.Write(p); err != nil {
		return n, err
	} else if n != sz {
		return n, io.ErrShortWrite
	}

	a.comp.write(p)
	return sz, nil
}

func (a *Analyser) ContentKey() ContentKey {
	return a.ckm.Sum(nil)
}

func (a *Analyser) Fingerprint() []byte {
	return a.fp.Sum(nil)
}

func (a *Analyser) Size() int {
	return a.comp.size
}

func (a *Analyser) CompressEstimate() float64 {

	return a.comp.estimate()
}

// See github.com/klauspost/compress@v1.17.8/compressible.go
type compressability struct {
	o1   [256]byte
	hist [256]int

	size      int
	hits      uint
	lastMatch bool
	c1        byte
}

func (c *compressability) write(p []byte) {
	for _, b := range p {
		if b == c.o1[c.c1] {
			// We only count a hit if there was two correct predictions in a row.
			if c.lastMatch {
				c.hits++
			}
			c.lastMatch = true
		} else {
			c.lastMatch = false
		}
		c.o1[c.c1] = b
		c.c1 = b
		c.hist[b]++
	}
	c.size += len(p)
}

func (c *compressability) estimate() float64 {
	if c.size < 16 {
		return 0
	}

	// Use x^0.6 to give better spread
	prediction := math.Pow(float64(c.hits)/float64(c.size), 0.6)

	// Calculate histogram distribution
	variance := float64(0)
	avg := float64(c.size) / 256

	for _, v := range c.hist {
		Δ := float64(v) - avg
		variance += Δ * Δ
	}

	stddev := math.Sqrt(float64(variance)) / float64(c.size)
	exp := math.Sqrt(1 / float64(c.size))

	// Subtract expected stddev
	stddev -= exp
	if stddev < 0 {
		stddev = 0
	}
	stddev *= 1 + exp

	// Use x^0.4 to give better spread
	entropy := math.Pow(stddev, 0.4)

	// 50/50 weight between prediction and histogram distribution
	return math.Pow((prediction+entropy)/2, 0.9)
}
