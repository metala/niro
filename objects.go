package niro

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"log"
	"strconv"

	"go.metala.org/niro/tnk"
)

var objectAbbrev = map[tnk.ObjectType]string{
	tnk.DataType:        "data",
	tnk.CompositeV0Type: "comp0",
	tnk.CompositeType:   "comp",
	tnk.TreeType:        "tree",
	tnk.ArchiveType:     "archive",
}

func (n *Niro) ReadObject(hash tnk.Hash) (*tnk.Header, io.ReadCloser, error) {
	if n.secretKey == nil {
		return nil, nil, errInvalidSecretKey
	}
	hdr, r, err := n.tnk.Open(hash, n.secretKey)
	return &hdr, r, err
}

func (n *Niro) ReadAllObject(hash tnk.Hash) (*tnk.Header, []byte, error) {
	hdr, rd, err := n.ReadObject(hash)
	if err != nil {
		return hdr, nil, err
	}
	defer rd.Close()

	data, err := io.ReadAll(rd)
	if err != nil {
		return hdr, nil, err
	}

	return hdr, data, nil
}

func (n *Niro) WriteObjectData(t tnk.ObjectType, data []byte) (*tnk.StoreResult, error) {
	if n.publicKey == nil {
		return nil, errMissingPublicKey
	}
	rd := io.Reader(bytes.NewReader(data))
	result, err := n.tnk.WriteObject(t, n.publicKey, rd)
	if err != nil {
		return nil, fmt.Errorf("Tnk.WriteObject: %w", err)
	}
	if n.Verbosity >= 2 {
		logHash(objectAbbrev[t], result.Hash, result.Size, result.Written, result.Exists)
	}
	return result, nil
}

func (n *Niro) ReadArchive(hash tnk.Hash) (*tnk.Archive, error) {
	_, r, err := n.tnk.Open(hash, n.secretKey)
	if err != nil {
		return nil, fmt.Errorf("open archive: %w", err)
	}
	defer r.Close()

	if isArchive, err := tnk.CheckMagic(r, tnk.ArchiveMagic); err != nil {
		return nil, err
	} else if !isArchive {
		return nil, errors.New("not an archive")
	}

	ar := tnk.Archive{}
	if err := ar.Unmarshal(r); err != nil {
		return nil, fmt.Errorf("failed to unmarshal archive: %w", err)
	}
	return &ar, nil
}

func logHash(t string, hash tnk.Hash, size, written uint64, exists bool) {
	var ratio uint64
	if size != 0 {
		ratio = (written * 100) / size
	}
	writtenStr := "exists"
	if !exists {
		writtenStr = strconv.FormatUint(written, 10)
	}

	log.Printf("%-7s %x -> %10d / %10s / %3d%% (size / written / ratio)", t, hash, size, writtenStr, ratio)
}
