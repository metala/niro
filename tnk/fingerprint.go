package tnk

import (
	"crypto/sha256"
	"hash"
	"io"

	"golang.org/x/crypto/blake2b"
	"golang.org/x/crypto/sha3"
)

type fingerprint struct {
	io.Writer

	h1 hash.Hash
	h2 hash.Hash
	h3 hash.Hash
}

func newFingerprint() hash.Hash {
	h1, _ := blake2b.New256(nil)
	h2 := sha256.New()
	h3 := sha3.New256()
	w := io.MultiWriter(h1, h2, h3)
	fp := &fingerprint{w, h1, h2, h3}
	return fp
}

func (f *fingerprint) Size() int {
	return 96
}

func (f *fingerprint) BlockSize() int {
	return 128
}

func (f *fingerprint) Reset() {
	f.h1.Reset()
	f.h2.Reset()
	f.h3.Reset()
}

func (f *fingerprint) Sum(b []byte) []byte {
	b = f.h1.Sum(b)
	b = f.h2.Sum(b)
	b = f.h3.Sum(b)
	return b
}
