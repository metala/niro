package main

import (
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"io"
	"log"
	"os"

	"github.com/spf13/cobra"
	"golang.org/x/crypto/curve25519"
)

func init() {
	rootCmd.AddCommand(keygenCmd)
	keygenCmd.Flags().BytesBase64VarP(&secretKey, "secret-key", "s", nil, `secret key in base64 format`)
}

var keygenCmd = &cobra.Command{
	Use:   "keygen",
	Short: "Generates a Curve25519 key-pair",
	Long:  `The commands generates a random key-pair by reading the cryto random.`,
	Run: func(cmd *cobra.Command, args []string) {
		var sk [32]byte
		if secretKey != nil {
			if len(secretKey) != 32 {
				log.Printf("invalid secret key")
				os.Exit(1)
			}
			copy(sk[:], secretKey)
		} else {
			_, err := io.ReadFull(rand.Reader, sk[:])
			if err != nil {
				log.Printf("failed to generate secret key: %s\n", err)
				os.Exit(2)
			}
		}
		pk, err := curve25519.X25519(sk[:], curve25519.Basepoint)
		if err != nil {
			log.Printf("failed to make public key: %s\n", err)
			os.Exit(3)
		}

		skb64 := base64.StdEncoding.EncodeToString(sk[:])
		fmt.Printf("Secret Key (sk): %s\n", skb64)

		pkb64 := base64.StdEncoding.EncodeToString(pk[:])
		fmt.Printf("Public Key (pk): %s\n", pkb64)
	},
}
