package tnk

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"os"
	"sync"

	"github.com/jotfs/fastcdc-go"
	"github.com/klauspost/compress/zstd"
	"golang.org/x/crypto/chacha20poly1305"
)

type Tnk struct {
	fs Fs

	// Keys, Cache and options
	kdf       Kdf
	repoSalt  []byte
	chunkOpts fastcdc.Options

	encoders *sync.Pool
}

type Opts struct {
	KdfPrefix   string
	RepoSalt    []byte
	Chunker     ChunkerOpts
	Compression CompressionOpts
}

type ChunkerOpts struct {
	MinSize uint
	AvgSize uint
	MaxSize uint
}

type CompressionOpts struct {
	Level       int
	LowerMem    bool
	Concurrency int
}

type Header struct {
	ObjectSize int64

	ContentFlags uint8
	Fingerprint  []byte
}

type StoreResult struct {
	Hash       Hash
	Size       uint64
	Written    uint64
	Compressed bool
	Exists     bool
}

var (
	errSameHashDifferentFingerprint = errors.New("same hash, different fingerprint")
)

func New(fs Fs, kdf Kdf, opts Opts) (*Tnk, error) {
	encoders := sync.Pool{
		New: func() any {
			zw, _ := zstd.NewWriter(nil,
				zstd.WithEncoderLevel(zstd.EncoderLevelFromZstd(opts.Compression.Level)),
				zstd.WithEncoderConcurrency(opts.Compression.Concurrency),
				zstd.WithLowerEncoderMem(opts.Compression.LowerMem))
			return zw
		},
	}
	return &Tnk{
		fs:       fs,
		kdf:      kdf,
		repoSalt: opts.RepoSalt,
		encoders: &encoders,

		chunkOpts: fastcdc.Options{
			MinSize:     int(opts.Chunker.MinSize),
			AverageSize: int(opts.Chunker.AvgSize),
			MaxSize:     int(opts.Chunker.MaxSize),
		},
	}, nil
}

func Defaults() Opts {
	return Opts{
		Chunker: ChunkerOpts{
			MinSize: 1 << 18, // 256KiB
			AvgSize: 1 << 19, // 512KiB
			MaxSize: 1 << 20, // 1MiB
		},
		Compression: CompressionOpts{
			Level:       3,
			Concurrency: 1,
		},
	}
}

func (t *Tnk) Open(hash Hash, sk *SecretKey) (hdr Header, r io.ReadCloser, err error) {
	p := hash.Path()
	st, err := t.fs.Stat(p)
	if err != nil {
		return hdr, nil, err
	}
	hdr.ObjectSize = st.Size()

	f, err := t.fs.Open(p)
	if err != nil {
		return hdr, nil, err
	}
	er, err := NewEncryptionReader(f, sk, t.kdf)
	if err != nil {
		return hdr, nil, err
	}
	hdr.Fingerprint = er.Fingerprint
	hdr.ContentFlags = er.Flags
	if hdr.ContentFlags&compressedFlag == 1 {
		r, err = NewCompressReader(er)
		if err != nil {
			er.Close()
			return hdr, nil, err
		}
	} else {
		r = er
	}
	return hdr, r, nil
}

func (t *Tnk) OpenReader(hash Hash, sk *SecretKey) (*objectReader, error) {
	r := &objectReader{
		tnk:  t,
		sk:   sk,
		hash: hash,
	}
	if err := r.Reset(); err != nil {
		return nil, err
	}
	return r, nil
}

func (t *Tnk) Store(r io.Reader, pk *PublicKey) (*StoreResult, error) {
	rs, seekable := r.(io.ReadSeeker)
	if !seekable {
		b, err := io.ReadAll(r)
		if err != nil {
			return nil, fmt.Errorf("non-seeakable ReadAll: %w", err)
		}
		rs = bytes.NewReader(b)
	}

	an := t.newAnalyser(pk)
	if _, err := io.Copy(an, rs); err != nil {
		return nil, fmt.Errorf("analyse: %w", err)
	}
	ck := an.ContentKey()
	kdf := boundKdf{
		kdf:        t.kdf,
		contentKey: ck,
		publicKey:  pk,
	}
	hash := kdf.Hash()
	size := an.Size()
	if size == 0 {
		return &StoreResult{
			Hash:   hash,
			Size:   uint64(size),
			Exists: true,
		}, nil
	}
	fp := an.Fingerprint()
	compress := an.CompressEstimate() > 0.1
	if _, err := rs.Seek(0, io.SeekStart); err != nil {
		return nil, fmt.Errorf("seek start: %w", err)
	}
	return t.store(hash, &kdf, fp, compress, size, rs)
}

func (t *Tnk) store(hash Hash, kdf *boundKdf, fingerprint []byte, compress bool, size int, r io.Reader) (*StoreResult, error) {
	f, err := t.create(hash)
	if err == os.ErrExist {
		if err := t.verifyFingerprint(hash, kdf, fingerprint); errors.Is(err, errSameHashDifferentFingerprint) {
			panic(err)
		} else if err != nil {
			return nil, fmt.Errorf("verify fingerprint for %x: %w", hash, err)
		}

		return &StoreResult{
			Hash:       hash,
			Size:       uint64(size),
			Exists:     true,
			Compressed: compress,
		}, nil
	} else if err != nil {
		return nil, err
	}

	w := io.WriteCloser(f)
	enc, err := NewEncryptionWriter(w, kdf)
	if err != nil {
		return nil, fmt.Errorf("encryption writer: %w", err)
	}

	// Write encryption header
	if _, err = enc.WriteHeader(); err != nil {
		return nil, fmt.Errorf("write header: %w", err)
	}

	flags := uint8(0x00)
	if compress {
		flags |= compressedFlag
	}

	// Write object fingerprint
	if _, err = enc.WriteMetadata(fingerprint, []byte{flags}); err != nil {
		return nil, fmt.Errorf("write metadata: %w", err)
	}

	w = enc
	if compress {
		encoder := t.encoders.Get().(*zstd.Encoder)
		defer encoder.Reset(nil)
		defer t.encoders.Put(encoder)
		comp := NewCompressWriter(w, encoder)
		w = comp
	}
	n, err := io.Copy(w, r)
	if err != nil {
		w.Close()
		return nil, fmt.Errorf("write data: %w", err)
	}
	if n != int64(size) {
		w.Close()
		return nil, fmt.Errorf("expected %d to be written, got %d", size, n)
	}
	if err = w.Close(); err != nil {
		return nil, fmt.Errorf("close: %w", err)
	}

	return &StoreResult{
		Hash:       hash,
		Size:       uint64(size),
		Written:    uint64(enc.written),
		Compressed: compress,
	}, nil
}

func (t *Tnk) verifyFingerprint(hash Hash, kdf *boundKdf, fingerprint []byte) error {
	mek := kdf.MetadataKey()
	mec, _ := chacha20poly1305.New(mek[:])
	ciphertext := make([]byte, 0, len(fingerprint)+chacha20poly1305.Overhead)
	ciphertext = mec.Seal(ciphertext[:0], zeroNonce[:], fingerprint, nil)

	f, err := t.fs.Open(hash.Path())
	if err != nil {
		return err
	}
	defer f.Close()

	data := make([]byte, 64+len(fingerprint)+(chacha20poly1305.Overhead*2))
	if _, err := io.ReadFull(f, data); err != nil {
		return err
	}
	if !bytes.Equal(data[64+chacha20poly1305.Overhead:], ciphertext) {
		return fmt.Errorf("%w: hash=%s", errSameHashDifferentFingerprint, hash.Hex())
	}
	return nil
}

func (t *Tnk) create(hash Hash) (File, error) {
	t.fs.Mkdir(hash.Dir(), 0o755)
	path := hash.Path()

	_, err := t.fs.Stat(path)
	if err == nil {
		return nil, os.ErrExist
	} else if !os.IsNotExist(err) {
		return nil, fmt.Errorf("stat: %w", err)
	}

	f, err := t.fs.OpenFile(path, os.O_WRONLY|os.O_CREATE, 0o644)
	if err != nil {
		return nil, fmt.Errorf("open wronly: %w", err)
	}
	return f, nil
}

func (t *Tnk) newAnalyser(pk *PublicKey) *Analyser {
	salt := make([]byte, 0, len(t.repoSalt)+len(pk))
	salt = append(salt, t.repoSalt...)
	salt = append(salt, pk[:]...)
	return NewAnalyser(t.kdf.ContentKeyMaker, newFingerprint, salt)
}
