package tnk

type HashFlags uint32

const (
	FlagComposite HashFlags = (1 << iota)
)

func (f HashFlags) IsSet(mask HashFlags) bool {
	return f&mask != 0
}
