package tnk

import (
	"bytes"
	"io"
	"time"

	"go.metala.org/niro/opis"
)

type Archive struct {
	Timestamp time.Time
	Hash      Hash
	HashFlags HashFlags
	Name      string
}

func (a *Archive) MarshalLen() int {
	return 8 + 32 + 4 + 2 + len(a.Name)
}

func (a *Archive) Marshal() []byte {
	b := make([]byte, 0, a.MarshalLen())
	buf := bytes.NewBuffer(b)
	opis.WriteMany(buf, a.Timestamp, []byte(a.Hash), uint32(a.HashFlags), a.Name)
	return buf.Bytes()
}

func (a *Archive) UnmarshalBytes(b []byte) error {
	r := bytes.NewBuffer(b)
	return a.Unmarshal(r)
}

func (a *Archive) Unmarshal(r io.Reader) error {
	var hash []byte
	var flags uint32

	err := opis.ReadMany(r, &a.Timestamp, &hash, &flags, &a.Name)
	if err != nil {
		return err
	}
	a.Hash = hash
	a.HashFlags = HashFlags(flags)

	return nil
}
