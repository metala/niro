package fusefs

import (
	"context"
	"io"
	"log"
	"syscall"

	"github.com/hanwen/go-fuse/v2/fs"
	"github.com/hanwen/go-fuse/v2/fuse"
	"go.metala.org/niro"
	"go.metala.org/niro/tnk"
)

// niroRegularFile is a filesystem node that holds a read-only data
// slice in memory.
type niroRegularFile struct {
	fs.Inode

	name      string
	niro      *niro.Niro
	Hash      tnk.Hash
	Composite bool

	Attr fuse.Attr
}

var _ = (fs.NodeOpener)((*niroRegularFile)(nil))
var _ = (fs.NodeReader)((*niroRegularFile)(nil))
var _ = (fs.NodeWriter)((*niroRegularFile)(nil))
var _ = (fs.NodeGetattrer)((*niroRegularFile)(nil))
var _ = (fs.NodeSetattrer)((*niroRegularFile)(nil))
var _ = (fs.NodeFlusher)((*niroRegularFile)(nil))

func (f *niroRegularFile) Open(ctx context.Context, flags uint32) (fh fs.FileHandle, fuseFlags uint32, errno syscall.Errno) {
	rsc, err := f.niro.ReadFile(f.Hash, f.Composite)
	if err != nil {
		log.Printf("error opening object '%s': %s", f.name, err)
		return nil, 0, syscall.EIO
	}
	return rsc, 0, fs.OK
}

func (f *niroRegularFile) Write(ctx context.Context, fh fs.FileHandle, data []byte, off int64) (uint32, syscall.Errno) {
	return 0, syscall.ENOSPC
}

func (f *niroRegularFile) Getattr(ctx context.Context, fh fs.FileHandle, out *fuse.AttrOut) syscall.Errno {
	out.Attr = f.Attr
	return fs.OK
}

func (f *niroRegularFile) Setattr(ctx context.Context, fh fs.FileHandle, in *fuse.SetAttrIn, out *fuse.AttrOut) syscall.Errno {
	return syscall.ENOSPC
}

func (f *niroRegularFile) Flush(ctx context.Context, fh fs.FileHandle) syscall.Errno {
	return 0
}

func (f *niroRegularFile) Read(ctx context.Context, fh fs.FileHandle, dest []byte, off int64) (fuse.ReadResult, syscall.Errno) {
	rsc := fh.(io.ReadSeekCloser)
	if n, err := rsc.Seek(off, io.SeekStart); err != nil {
		log.Printf("failed to seek at %d on '%s', stopped at %d: %s", off, f.name, n, err)
		return nil, syscall.EIO
	}
	if n, err := io.ReadFull(rsc, dest); err != nil && err != io.EOF && err != io.ErrUnexpectedEOF {
		log.Printf("failed to read %d bytes from '%s', stopped at %d: %s", len(dest), f.name, n, err)
		return nil, syscall.EIO
	}

	return fuse.ReadResultData(dest), fs.OK
}

// MemSymlink is an inode holding a symlink in memory.
type MemSymlink struct {
	fs.Inode
	Attr fuse.Attr
	Data []byte
}

var _ = (fs.NodeReadlinker)((*MemSymlink)(nil))
var _ = (fs.NodeGetattrer)((*MemSymlink)(nil))

func (l *MemSymlink) Readlink(ctx context.Context) ([]byte, syscall.Errno) {
	return l.Data, fs.OK
}

func (l *MemSymlink) Getattr(ctx context.Context, fh fs.FileHandle, out *fuse.AttrOut) syscall.Errno {
	out.Attr = l.Attr
	return fs.OK
}

type memDir struct {
	fs.Inode

	Attr fuse.Attr
}

var _ = (fs.NodeUnlinker)((*memDir)(nil))
var _ = (fs.NodeRmdirer)((*memDir)(nil))
var _ = (fs.NodeRenamer)((*memDir)(nil))

func (f *memDir) Unlink(ctx context.Context, name string) syscall.Errno {
	return syscall.ENOSPC
}

func (f *memDir) Rmdir(ctx context.Context, name string) syscall.Errno {
	return syscall.ENOSPC
}

func (f *memDir) Rename(ctx context.Context, name string, newParent fs.InodeEmbedder, newName string, flags uint32) syscall.Errno {
	return syscall.ENOSPC
}
