package tnk

import (
	"io"

	"github.com/klauspost/compress/zstd"
)

const (
	compressedFlag uint8 = (1 << iota)
)

type CompressWriter struct {
	zw io.WriteCloser
	c  io.Closer
}

func NewCompressWriter(w io.WriteCloser, zw *zstd.Encoder) *CompressWriter {
	zw.Reset(w)
	return &CompressWriter{zw, w}
}

func (c *CompressWriter) Write(p []byte) (n int, err error) {
	return c.zw.Write(p)
}

func (c *CompressWriter) Close() error {
	if err := c.zw.Close(); err != nil {
		return err
	}
	return c.c.Close()
}

type CompressReader struct {
	zr *zstd.Decoder
	c  io.Closer
}

func NewCompressReader(r io.ReadCloser) (*CompressReader, error) {
	zr, err := zstd.NewReader(r, zstd.WithDecoderLowmem(true))
	if err != nil {
		return nil, err
	}
	return &CompressReader{zr, r}, nil
}

func (c *CompressReader) Read(p []byte) (n int, err error) {
	return c.zr.Read(p)
}

func (c *CompressReader) Close() error {
	c.zr.Close()
	return c.c.Close()
}
