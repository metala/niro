package main

import (
	"fmt"
	"os"

	"github.com/spf13/afero"
	"github.com/spf13/cobra"
	"go.metala.org/niro"
)

func init() {
	listCmd.Flags().StringVarP(&repositoryPath, "repository", "r", "", "/path/to/repository")
	listCmd.MarkFlagRequired("repository")
	listCmd.Flags().BytesBase64VarP(&secretKey, "secret-key", "s", nil, "secret key in base64")
	listCmd.MarkFlagRequired("secret-key")
	rootCmd.AddCommand(listCmd)
}

var listCmd = &cobra.Command{
	Use:   "list",
	Short: "List archives",
	Long:  `List command walks over all the objects, filter the archives lists them with some information.`,
	Args:  cobra.ExactArgs(0),
	Run: func(cmd *cobra.Command, args []string) {
		repoFs := afero.NewBasePathFs(afero.NewOsFs(), repositoryPath)
		repo, err := niro.Existing(repoFs)
		if err != nil {
			fmt.Fprintf(os.Stderr, "failed to open repository: %s\n", err)
			os.Exit(2)
		}
		if err = repo.SetSecretKey(secretKey); err != nil {
			fmt.Fprintf(os.Stderr, "invalid secret key: %s\n", err)
			os.Exit(4)
		}

		ch, errc := repo.ListArchives()
		n := 0
		for info := range ch {
			n += 1

			fmt.Printf("====\n")
			fmt.Printf("Hash: %x\n", info.Hash)
			fmt.Printf("Name: %s\n", info.Archive.Name)
			fmt.Printf("Timestamp: %s\n", info.Archive.Timestamp)
			fmt.Printf("Tree: %x\n", info.Archive.Hash)
			fmt.Printf("Tree Flags: %x\n", info.Archive.HashFlags)
			fmt.Printf("\n")
		}
		if n == 0 {
			fmt.Println("<no archives found>")
		}

		if err = <-errc; err != nil {
			fmt.Fprintf(os.Stderr, "operation failed: %s\n", err)
			os.Exit(3)
		}

	},
}
