package niro

import (
	"errors"
	"fmt"

	"github.com/pelletier/go-toml/v2"
	"github.com/spf13/afero"
	"go.metala.org/niro/config"
	"go.metala.org/niro/tnk"
)

var (
	Version string = "hatchet"

	errInvalidRepoSalt  = errors.New("expects 32 bytes repository salt")
	errInvalidPublicKey = errors.New("expects 32 bytes public key")
	errInvalidSecretKey = errors.New("expects 32 bytes secret key")
)

type Niro struct {
	format uint

	repoFs afero.Fs
	tnk    *tnk.Tnk

	// Operational Keys
	repoSalt  [32]byte
	secretKey *[32]byte
	publicKey *[32]byte

	// Options
	Verbosity   uint
	Concurrency uint
}

func New(repoFs afero.Fs) (*Niro, error) {
	n := Niro{
		format: 1,
		repoFs: repoFs,

		// Options
		Verbosity: 0,
	}
	if err := n.setUpTnk(); err != nil {
		return nil, err
	}
	return &n, nil
}

func Existing(repoFs afero.Fs) (*Niro, error) {
	n, err := New(repoFs)
	if err != nil {
		return nil, err
	}
	if err := n.LoadConfig(); err != nil {
		return nil, fmt.Errorf("load config: %w", err)
	}
	return n, nil
}

func (n *Niro) LoadConfig() error {
	cfg, err := n.readConfig()
	if err != nil {
		return err
	}
	return n.SetConfig(cfg)
}

func (n *Niro) SetConfig(cfg *config.Config) error {
	if len(cfg.Salt) != 32 {
		return errInvalidRepoSalt
	}
	copy(n.repoSalt[:], cfg.Salt)
	return nil
}

func (n *Niro) readConfig() (*config.Config, error) {
	f, err := n.repoFs.Open("config")
	if err != nil {
		return nil, fmt.Errorf("open config: %w", err)
	}
	cfg := config.New()
	err = toml.NewDecoder(f).Decode(cfg)
	if err != nil {
		return nil, fmt.Errorf("decode config: %w", err)
	}
	return cfg, nil
}

func (n *Niro) setUpTnk() (err error) {
	opts := tnk.Defaults()
	opts.RepoSalt = n.repoSalt[:]
	opts.KdfPrefix = kdfPrefixContext
	opts.Compression = tnk.CompressionOpts{
		Level:       3,
		LowerMem:    true,
		Concurrency: int(n.Concurrency),
	}
	fs := &tnkAferoFs{afero.NewBasePathFs(n.repoFs, "objects")}
	kdf := tnk.NewKdf(kdfPrefixContext)
	if n.tnk, err = tnk.New(fs, kdf, opts); err != nil {
		return err
	}

	return nil
}
