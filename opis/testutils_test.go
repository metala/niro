package opis

type errReader struct{ error }

func (e *errReader) Read(b []byte) (n int, err error) {
	return 0, e.error
}

type errWriter struct {
	err     error
	discard int
}

func (e *errWriter) Write(b []byte) (n int, err error) {
	max := len(b)
	if max > e.discard {
		max = e.discard
	}
	e.discard -= max
	if max == 0 {
		return 0, e.err
	}
	return max, nil
}
