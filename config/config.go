package config

import (
	cryptorand "crypto/rand"
	"io"
)

type Config struct {
	Format uint        `toml:"format"`
	Salt   base64Field `toml:"salt"`
}

func New() *Config {
	return &Config{}
}

func (c *Config) SetRandomSalt() error {
	salt := make([]byte, 32)
	if _, err := io.ReadFull(cryptorand.Reader, salt); err != nil {
		return err
	}
	c.Salt = salt

	return nil
}
