package opis

import (
	"bytes"
	"errors"
	"io"
	"strings"
	"testing"
	"time"

	"github.com/matryer/is"
)

func TestReadMany(t *testing.T) {
	data := []byte{
		0xff, 0xf0, // int16(-16)
		0xff, 0xff, 0xff, 0xe0, // int32(-32)
		0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xc0, // int64(-64)

		0x00, 0x10, // uint16(16)
		0x00, 0x00, 0x00, 0x20, // uint32(32)
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, // uint64(64)

		0x00, 6, 's', 't', 'r', 'i', 'n', 'g', // "string"
		0x00, 8, 1, 2, 3, 4, 5, 6, 7, 8, // []byte{1, 2, 3, 4, 5, 6, 7, 8}

		// [32]byte
		0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
		0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f,

		0x00, 0x00, 0x00, 0x00, 0x00, 0x36, 0xee, 0x80, // time.Date(1970, 1, 1, 1, 0, 0, 0, time.UTC) == int64(3600_000)
	}
	r := bytes.NewReader(data)
	var (
		i16     int16
		i32     int32
		i64     int64
		u16     uint16
		u32     uint32
		u64     uint64
		s       string
		b       []byte
		bytes32 [32]byte
		tm      time.Time
	)
	err := ReadMany(r,
		&i16, &i32, &i64,
		&u16, &u32, &u64,
		&s,
		&b,
		&bytes32,
		&tm,
	)
	is := is.New(t)
	is.NoErr(err)
	is.Equal(i16, int16(-16))
	is.Equal(i32, int32(-32))
	is.Equal(i64, int64(-64))
	is.Equal(u16, uint16(16))
	is.Equal(u32, uint32(32))
	is.Equal(u64, uint64(64))
	is.Equal(s, "string")
	is.Equal(b, []byte{1, 2, 3, 4, 5, 6, 7, 8})
	is.Equal(bytes32, [32]byte{
		0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
		0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f,
	})
	is.Equal(tm, time.Date(1970, 1, 1, 1, 0, 0, 0, time.UTC))
}

func TestReadNilBytes(t *testing.T) {
	data := []byte{0x00, 0x00}
	r := bytes.NewReader(data)
	var b []byte
	err := ReadBytes(r, &b)
	is := is.New(t)
	is.NoErr(err)
	is.Equal(b, []byte{})
}

func TestReadWrongType(t *testing.T) {
	buf := bytes.NewBuffer(nil)
	var i int
	err := Read(buf, &i)

	is := is.New(t)
	is.True(err != nil)
	is.True(strings.HasPrefix(err.Error(), "unsupported type"))
}

func TestReadMultiStringFailLen(t *testing.T) {
	expectedErr := errors.New("error reading from device")
	r := errReader{expectedErr}
	var s string
	err := ReadMany(&r, &s)

	is := is.New(t)
	is.True(err != nil)
	is.Equal(err.Error(), "read arg #0 error: read len error: error reading from device")
}

func TestReadMultiStringFailBytes(t *testing.T) {
	expectedErr := errors.New("error reading from device")
	r := errReader{expectedErr}
	mrd := io.MultiReader(bytes.NewReader([]byte{0, 1}), &r)
	var s string
	err := ReadMany(mrd, &s)

	is := is.New(t)
	is.True(err != nil)
	is.Equal(err.Error(), "read arg #0 error: read bytes error: error reading from device")
}

func TestReadTimeFail(t *testing.T) {
	expectedErr := errors.New("error reading from device")
	r := errReader{expectedErr}
	mrd := io.MultiReader(bytes.NewReader([]byte{0}), &r)
	var ts time.Time
	err := ReadTime(mrd, &ts)

	is := is.New(t)
	is.True(err != nil)
	is.Equal(err.Error(), "read time: error reading from device")
}

func TestRead32BytesFail(t *testing.T) {
	expectedErr := errors.New("error reading from device")
	r := errReader{expectedErr}
	mrd := io.MultiReader(bytes.NewReader([]byte{0}), &r)
	var bytes32 [32]byte
	err := Read32Bytes(mrd, &bytes32)

	is := is.New(t)
	is.True(err != nil)
	is.Equal(err.Error(), "read 32 bytes: error reading from device")
}
