package niro

import (
	"os"

	"github.com/spf13/afero"
	"go.metala.org/niro/tnk"
)

type tnkAferoFs struct {
	afero.Fs
}

func (tfs *tnkAferoFs) Create(name string) (tnk.File, error) {
	f, err := tfs.Fs.Create(name)
	if err != nil {
		return nil, err
	}
	return tnk.File(f), nil
}

func (tfs *tnkAferoFs) Open(name string) (tnk.File, error) {
	f, err := tfs.Fs.Open(name)
	if err != nil {
		return nil, err
	}
	return tnk.File(f), nil
}

func (tfs *tnkAferoFs) OpenFile(name string, flag int, perm os.FileMode) (tnk.File, error) {
	f, err := tfs.Fs.OpenFile(name, flag, perm)
	if err != nil {
		return nil, err
	}
	return tnk.File(f), nil
}
