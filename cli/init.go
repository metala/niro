package main

import (
	"log"
	"os"

	"github.com/spf13/afero"
	"github.com/spf13/cobra"
	"go.metala.org/niro"
)

func init() {
	initCmd.Flags().StringVarP(&repositoryPath, "repository", "r", "", `/path/to/repository`)
	initCmd.MarkFlagRequired("repository")
	rootCmd.AddCommand(initCmd)
}

var initCmd = &cobra.Command{
	Use:   "init",
	Short: "Initializes backup repository",
	Long:  `Init create a repositry configuration and seeds it with random salt.`,
	Run: func(cmd *cobra.Command, args []string) {
		repoFs := afero.NewBasePathFs(afero.NewOsFs(), repositoryPath)
		r, err := niro.New(repoFs)
		if err != nil {
			log.Printf("failed to setup niro: %s\n", err)
		}
		if err := r.Init(); err != nil {
			log.Printf("failed to init repository: %s\n", err)
			os.Exit(3)
		}
	},
}
