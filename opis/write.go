package opis

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
	"time"
)

func WriteMany(w io.Writer, args ...interface{}) error {
	for i, arg := range args {
		if err := Write(w, arg); err != nil {
			return fmt.Errorf("write arg #%d: %w", i, err)
		}
	}
	return nil
}

func Write(w io.Writer, in interface{}) error {
	switch v := in.(type) {
	case []byte:
		return WriteBytes(w, v)
	case string:
		return WriteString(w, v)
	case int16, int32, int64,
		uint16, uint32, uint64:
		return binary.Write(w, binary.BigEndian, v)
	case time.Time:
		return WriteTime(w, v)
	case [32]byte:
		return Write32Bytes(w, v)
	}
	return fmt.Errorf("unsupported type '%T'", in)
}

func WriteString(w io.Writer, s string) error {
	return WriteBytes(w, []byte(s))
}

func WriteBytes(w io.Writer, b []byte) error {
	if err := binary.Write(w, binary.BigEndian, uint16(len(b))); err != nil {
		return fmt.Errorf("write len: %w", err)
	}
	sr := bytes.NewReader(b)
	if _, err := io.Copy(w, sr); err != nil {
		return fmt.Errorf("write bytes: %w", err)
	}

	return nil
}

func Write32Bytes(w io.Writer, b [32]byte) error {
	sr := bytes.NewReader(b[:])
	if _, err := io.Copy(w, sr); err != nil {
		return fmt.Errorf("write 32 bytes: %w", err)
	}

	return nil
}

func WriteTime(w io.Writer, t time.Time) error {
	return Write(w, int64(t.UnixMilli()))
}
