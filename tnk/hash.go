package tnk

import (
	"encoding/hex"
	"fmt"
)

const HashSize = 32

type Hash []byte

func (h Hash) Hex() string {
	return hex.EncodeToString(h)
}

func (h Hash) Dir() string {
	return fmt.Sprintf("%x", h[0:1])
}

func (h Hash) Path() string {
	return fmt.Sprintf("%x/%x", h[0:1], h[1:])
}
