package niro

import (
	"errors"

	"golang.org/x/crypto/curve25519"
)

const (
	kdfPrefixContext = "niro 1660355200"
)

var (
	errMissingPublicKey = errors.New("missing public key")
)

func (n *Niro) SetPublicKey(pk []byte) error {
	if len(pk) != 32 {
		return errInvalidPublicKey
	}

	n.publicKey = &[32]byte{}
	copy(n.publicKey[:], pk)
	if err := n.setUpTnk(); err != nil {
		return err
	}

	return nil
}

func (n *Niro) SetSecretKey(sk []byte) error {
	if len(sk) != 32 {
		return errInvalidSecretKey
	}

	n.secretKey = &[32]byte{}
	copy(n.secretKey[:], sk)
	if n.publicKey != nil {
		return nil
	}

	n.publicKey = &[32]byte{}
	curve25519.ScalarBaseMult(n.publicKey, n.secretKey)
	if err := n.setUpTnk(); err != nil {
		return err
	}
	return nil
}
