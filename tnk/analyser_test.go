package tnk

import (
	"testing"

	"github.com/matryer/is"
)

var (
	defaultTnkKdf = &tnkKdf{
		content:     "content",
		hash:        "hash",
		dataKey:     "dataKey",
		metadataKey: "metadataKey",
	}
	salt = []byte("salt")
)

func TestNewHasher(t *testing.T) {
	NewAnalyser(defaultTnkKdf.ContentKeyMaker, newFingerprint, salt)
}

func TestHashBytes(t *testing.T) {
	is := is.New(t)

	a := NewAnalyser(defaultTnkKdf.ContentKeyMaker, newFingerprint, salt)
	a.Write([]byte("test"))
	ck := a.ContentKey()
	hash := defaultTnkKdf.Hash(ck)
	is.Equal(hash.Hex(), "a2b3b4759c58dee4c2afb4e5a19d2087dcb24cd5bbede8f180bd7bd13a2d96c5")
}

func TestHashFingerprint(t *testing.T) {
	is := is.New(t)

	a := NewAnalyser(defaultTnkKdf.ContentKeyMaker, newFingerprint, salt)
	_, err := a.Write([]byte("test"))
	is.NoErr(err)
	ck := a.ContentKey()
	hash := defaultTnkKdf.Hash(ck)
	is.Equal(hash.Hex(), "a2b3b4759c58dee4c2afb4e5a19d2087dcb24cd5bbede8f180bd7bd13a2d96c5")
	is.Equal(a.Fingerprint(), []byte{
		146, 139, 32, 54, 105, 67, 226, 175, 209, 30, 188, 14, 174, 46, 83, 169,
		59, 241, 119, 164, 252, 243, 91, 204, 100, 213, 3, 112, 78, 101, 226, 2,
		159, 134, 208, 129, 136, 76, 125, 101, 154, 47, 234, 160, 197, 90, 208, 21,
		163, 191, 79, 27, 43, 11, 130, 44, 209, 93, 108, 21, 176, 240, 10, 8,
		54, 240, 40, 88, 11, 176, 44, 200, 39, 42, 154, 2, 15, 66, 0, 227,
		70, 226, 118, 174, 102, 78, 69, 238, 128, 116, 85, 116, 226, 245, 171, 128,
	})
	dek := defaultTnkKdf.DataKey(ck)
	is.Equal(dek, [32]byte{
		25, 196, 8, 224, 34, 77, 222, 57, 211, 104, 71, 3, 30, 154, 114, 58,
		66, 29, 124, 4, 72, 16, 15, 97, 163, 152, 230, 121, 163, 18, 190, 220,
	})
}
