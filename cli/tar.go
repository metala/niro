package main

import (
	"encoding/hex"
	"fmt"
	"io"
	"os"

	"github.com/klauspost/readahead"
	"github.com/spf13/afero"
	"github.com/spf13/cobra"
	"go.metala.org/niro"
)

func init() {
	tarImportCmd.Flags().StringVarP(&repositoryPath, "repository", "r", "", `/path/to/repository`)
	tarImportCmd.MarkFlagRequired("repository")
	tarImportCmd.Flags().BytesBase64VarP(&publicKey, "public-key", "p", nil, `public key in base64`)
	tarImportCmd.MarkFlagRequired("public-key")
	tarImportCmd.Flags().StringVarP(&name, "name", "n", "", `archive name`)
	tarImportCmd.MarkFlagRequired("name")
	tarImportCmd.Flags().StringVarP(&input, "input", "i", "", `input file (use "-" for stdin)`)
	tarImportCmd.MarkFlagRequired("input")
	tarImportCmd.Flags().UintVarP(&concurrency, "concurrency", "c", 1, `write / compression concurrency`)

	tarExportCmd.Flags().StringVarP(&repositoryPath, "repository", "r", "", "/path/to/repository")
	tarExportCmd.MarkFlagRequired("repository")
	tarExportCmd.Flags().BytesBase64VarP(&secretKey, "secret-key", "s", nil, `secret key in base64`)
	tarExportCmd.MarkFlagRequired("secret-key")
	tarExportCmd.Flags().StringVarP(&hash, "hash", "H", "", `archive hash`)
	tarExportCmd.MarkFlagRequired("hash")
	tarExportCmd.Flags().StringVarP(&output, "output", "o", "", `output file (use "-" for stdout)`)
	tarExportCmd.MarkFlagRequired("output")

	tarCmd.AddCommand(tarImportCmd)
	tarCmd.AddCommand(tarExportCmd)
	rootCmd.AddCommand(tarCmd)
}

var tarCmd = &cobra.Command{
	Use:   "tar",
	Short: "Tar archive operations",
	Long:  `Import / export tar archive.`,
}

var tarImportCmd = &cobra.Command{
	Use:   "import",
	Short: "Imports a tar archive",
	Long:  `Import command reads a tar archive and imports in in the repository as a new archive.`,
	Args:  cobra.ExactArgs(0),
	Run: func(cmd *cobra.Command, args []string) {
		repoFs := afero.NewBasePathFs(afero.NewOsFs(), repositoryPath)
		repo, err := niro.Existing(repoFs)
		if err != nil {
			fmt.Fprintf(os.Stderr, "failed to open repository: %s\n", err)
			os.Exit(2)
		}
		repo.Verbosity = uint(verbosity)
		repo.Concurrency = concurrency
		err = repo.SetPublicKey(publicKey)
		if err != nil {
			fmt.Fprintf(os.Stderr, "invalid public key: %s\n", err)
			os.Exit(3)
		}

		var rd io.Reader
		if input == "-" {
			rd = os.Stdin
		} else {
			hostFs := afero.NewOsFs()
			rd, err = hostFs.Open(input)
			if err != nil {
				fmt.Fprintf(os.Stderr, "failed to open tar archive: %s\n", err)
				os.Exit(4)
			}
		}

		ra := readahead.NewReader(rd)
		defer ra.Close()
		_, hash, err := repo.TarImport(ra, name)
		if err != nil {
			fmt.Fprintf(os.Stderr, "operation failed: %s\n", err)
			os.Exit(3)
		}
		fmt.Printf("Created archive with hash: %x\n", hash)
	},
}

var tarExportCmd = &cobra.Command{
	Use:   "export",
	Short: "Export a tar archive",
	Long:  `Export command writes a tar archive from a repository archive.`,
	Args:  cobra.ExactArgs(0),
	Run: func(cmd *cobra.Command, args []string) {
		repoFs := afero.NewBasePathFs(afero.NewOsFs(), repositoryPath)
		repo, err := niro.Existing(repoFs)
		if err != nil {
			fmt.Fprintf(os.Stderr, "failed to open repository: %s\n", err)
			os.Exit(2)
		}
		repo.Verbosity = uint(verbosity)
		err = repo.SetSecretKey(secretKey)
		if err != nil {
			fmt.Fprintf(os.Stderr, "invalid secret key: %s\n", err)
			os.Exit(2)
		}
		h, err := hex.DecodeString(hash)
		if err != nil {
			fmt.Fprintf(os.Stderr, "invalid hash: %s\n", err)
			os.Exit(2)
		}

		var wr io.WriteCloser
		if output == "-" {
			wr = os.Stdout
		} else {
			hostFs := afero.NewOsFs()
			wr, err = hostFs.OpenFile(output, os.O_WRONLY|os.O_CREATE, 0o644)
			if err != nil {
				fmt.Fprintf(os.Stderr, "failed to open tar archive: %s\n", err)
				os.Exit(4)
			}
		}

		err = repo.TarExport(wr, h)
		if err != nil {
			fmt.Fprintf(os.Stderr, "operation failed: %s\n", err)
			os.Exit(10)
		}
	},
}
