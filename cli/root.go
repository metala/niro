package main

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var (
	repositoryPath string
	input          string
	output         string
	name           string
	hash           string
	secretKey      []byte
	publicKey      []byte
	concurrency    uint
	verbosity      int
)

func init() {
	rootCmd.PersistentFlags().CountVarP(&verbosity, "verbose", "v", "add to increase verbosity")
}

var rootCmd = &cobra.Command{
	Use:   "niro",
	Short: "Niro is a backup tool with built-in deduplication and security features",
	Long:  `Niro is backup utility that features deduplication and asymmetric encryption`,
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help() // Show all commands
	},
}

// Execute calls the root command
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
