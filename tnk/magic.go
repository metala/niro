package tnk

import (
	"bytes"
	"io"
)

var (
	CompositeV0Magic = []byte{0x1a, 0x11, 0xb2, 0xb3}
	CompositeMagic   = []byte{0x5d, 0x51, 0xbe, 0x97}
	TreeMagic        = []byte{0x29, 0x84, 0xa8, 0x65}
	ArchiveMagic     = []byte{0xb3, 0x12, 0x73, 0x56}
)

var (
	MagicMap = map[ObjectType][]byte{
		CompositeV0Type: CompositeV0Magic,
		CompositeType:   CompositeMagic,
		TreeType:        TreeMagic,
		ArchiveType:     ArchiveMagic,
	}
)

func ReadMagic(r io.Reader) ([]byte, error) {
	magic := make([]byte, 4)
	if _, err := io.ReadFull(r, magic); err != nil {
		return nil, err
	}
	return magic, nil
}

func CheckMagic(r io.Reader, magic []byte) (bool, error) {
	data, err := ReadMagic(r)
	if err != nil {
		return false, err
	}
	return bytes.Equal(data, magic), nil
}
