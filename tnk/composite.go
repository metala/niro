package tnk

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"sort"

	"go.metala.org/niro/opis"
)

type compositeReader struct {
	tnk     *Tnk
	sk      *SecretKey
	hashes  []Hash
	size    uint64
	offsets []int64
	indexed bool

	r   io.ReadCloser
	cur int
	pos int64
}

type Chunk struct {
	Hash    Hash
	Size    uint32
	Written uint32
	Exists  bool
}

func NewCompositeReader(tnk *Tnk, sk *SecretKey, r io.Reader) (*compositeReader, error) {
	cr := compositeReader{tnk: tnk, sk: sk}
	if err := cr.setup(r); err != nil {
		return nil, err
	}
	return &cr, nil
}

func (r *compositeReader) setup(rd io.Reader) error {
	magic, err := ReadMagic(rd)
	if err != nil {
		return err
	}

	data, err := io.ReadAll(rd)
	if err != nil {
		return err
	}

	var count uint32
	var compositeV0 bool
	var off int
	switch {
	case bytes.Equal(magic, CompositeMagic):
		count = binary.BigEndian.Uint32(data[:4])
		off = 4
	case bytes.Equal(magic, CompositeV0Magic):
		if len(data)%HashSize != 0 {
			return errors.New("invalid composition size")
		}
		count = uint32(len(data) / HashSize)
		compositeV0 = true
	default:
		return errors.New("not a composite file")
	}

	r.hashes = make([]Hash, count)
	for i := uint32(0); i < count; i++ {
		r.hashes[i] = data[off : off+HashSize]
		off += HashSize
	}

	if !compositeV0 {
		totalOffset := uint64(0)
		r.offsets = make([]int64, count)
		for i := uint32(0); i < count; i++ {
			size := binary.BigEndian.Uint32(data[off : off+4])
			r.offsets[i] = int64(totalOffset)
			totalOffset += uint64(size)
			off += 4 /*uint32*/
		}
		r.size = totalOffset
		r.indexed = true
	}

	if err := r.reset(); err != nil {
		return err
	}
	return nil
}

func (r *compositeReader) reset() error {
	r.Close()
	r.r = nil
	if err := r.load(0); err != nil {
		return err
	}
	return nil
}

func (r *compositeReader) load(idx int) (err error) {
	r.r, err = r.tnk.OpenReader(r.hashes[idx], r.sk)
	if err != nil {
		return err
	}
	r.cur = idx
	if r.offsets != nil {
		r.pos = r.offsets[idx]
	} else if idx == 0 {
		r.pos = 0
	}
	return nil
}

func (r *compositeReader) Read(p []byte) (n int, err error) {
	for r.r != nil {
		n, err = r.r.Read(p)
		r.pos += int64(n)

		if err == io.EOF {
			r.r.Close()
			r.r = nil
			if r.cur+1 < len(r.hashes) {
				err = r.load(r.cur + 1)
			}
		}
		if n > 0 || err != nil {
			return n, err
		}
	}
	return 0, io.EOF
}

func (r *compositeReader) Seek(offset int64, whence int) (int64, error) {
	switch whence {
	default:
		return 0, errWhence
	case io.SeekStart:
		offset += 0
	case io.SeekCurrent:
		offset += r.pos
		// case io.SeekEnd:
		// 	offset += int64(r.size)
	}
	if offset < 0 /*|| offset >= int64(r.size)*/ {
		return 0, errOffset
	}
	if offset == r.pos {
		return offset, nil
	}

	if !r.indexed {
		if offset < r.pos {
			if err := r.reset(); err != nil {
				return 0, err
			}
		}
		// seek / read forward
		_, err := io.CopyN(io.Discard, r, offset-r.pos)
		return offset, err
	}

	// else if indexed
	chunkIdx := sort.Search(len(r.offsets), func(i int) bool {
		return offset <= r.offsets[i]
	})
	chunkIdx -= 1
	if chunkIdx != r.cur || offset < r.pos {
		if err := r.Close(); err != nil {
			return 0, err
		}
		if err := r.load(chunkIdx); err != nil {
			return 0, err
		}
	}
	if n, err := io.CopyN(io.Discard, r.r, offset-r.pos); err != nil {
		r.pos += n
		return r.pos, err
	}
	r.pos = offset
	return r.pos, nil
}

func (r *compositeReader) Close() error {
	if r.r == nil {
		return nil
	}
	return r.r.Close()
}

func (n *Tnk) WriteComposite(pk *PublicKey, chunks []Chunk) (*StoreResult, error) {
	datalen := 4 /*int32*/ + (len(chunks) * (HashSize + 4 /*int32*/))
	buf := bytes.NewBuffer(make([]byte, 0, datalen))
	err := opis.Write(buf, uint32(len(chunks)))
	if err != nil {
		return nil, fmt.Errorf("opis write: %w", err)
	}
	for _, chunk := range chunks {
		// no error check. it will panic if too large
		buf.Write(chunk.Hash)
	}
	for _, chunk := range chunks {
		// no error check. it will panic if too large
		opis.Write(buf, chunk.Size)
	}

	r := bytes.NewReader(buf.Bytes())
	result, err := n.WriteObject(CompositeType, pk, r)
	if err != nil {
		return nil, fmt.Errorf("write object: %w", err)
	}
	return result, err
}
