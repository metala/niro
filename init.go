package niro

import (
	"errors"
	"fmt"

	"github.com/pelletier/go-toml/v2"
	"github.com/spf13/afero"
	"go.metala.org/niro/config"
)

var (
	errConfigExists = errors.New("repository config exists")
)

func (n *Niro) initConfig() error {
	_, err := n.repoFs.Stat("config")
	if err == nil {
		return errConfigExists
	}

	f, err := n.repoFs.Create("config")
	if err != nil {
		return fmt.Errorf("create: %w", err)
	}
	defer f.Close()

	cfg := config.New()
	cfg.Format = n.format
	if err = cfg.SetRandomSalt(); err != nil {
		return fmt.Errorf("config.GenerateKey(): %w", err)
	}
	if err = toml.NewEncoder(f).Encode(cfg); err != nil {
		return fmt.Errorf("write config: %w", err)
	}

	return nil
}

func (n *Niro) initLayout() error {
	directories := []string{"objects"}

	for _, dirname := range directories {
		exists, err := afero.DirExists(n.repoFs, dirname)
		if err != nil {
			return fmt.Errorf("directory error '%s': %w", dirname, err)
		} else if exists {
			continue
		}
		err = n.repoFs.Mkdir(dirname, 0o755)
		if err != nil {
			return fmt.Errorf("create directory '%s': %w", dirname, err)
		}
	}

	return nil
}

func (n *Niro) Init() error {
	n.repoFs.Mkdir("", 0o755)

	err := n.initConfig()
	if err != nil {
		return fmt.Errorf("config: %w", err)
	}
	err = n.initLayout()
	if err != nil {
		return fmt.Errorf("repository layout: %w", err)
	}

	return nil
}
