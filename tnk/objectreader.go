package tnk

import (
	"errors"
	"io"
)

var errWhence = errors.New("Seek: invalid whence")
var errOffset = errors.New("Seek: invalid offset")

type objectReader struct {
	tnk  *Tnk
	sk   *SecretKey
	hash Hash

	rc  io.ReadCloser
	pos int64
}

func (r *objectReader) Reset() error {
	if err := r.Close(); err != nil {
		return err
	}

	_, rc, err := r.tnk.Open(r.hash, r.sk)
	if err != nil {
		return err
	}

	r.pos = 0
	r.rc = rc
	return nil
}

func (r *objectReader) Close() error {
	if r.rc == nil {
		return nil
	}

	return r.rc.Close()
}

func (r *objectReader) Read(p []byte) (n int, err error) {
	n, err = r.rc.Read(p)
	r.pos += int64(n)
	return n, err
}

func (r *objectReader) Seek(offset int64, whence int) (int64, error) {
	switch whence {
	default:
		return 0, errWhence
	case io.SeekStart:
		offset += 0
	case io.SeekCurrent:
		offset += r.pos
	case io.SeekEnd:
		return r.pos, errWhence
	}
	if offset < 0 {
		return 0, errOffset
	}

	if offset < r.pos {
		if err := r.Reset(); err != nil {
			return r.pos, err
		}
	} else {
		offset -= r.pos
	}

	n, err := io.CopyN(io.Discard, r.rc, offset)
	r.pos += n
	if err != nil {
		return r.pos, err
	}
	return r.pos, nil
}
