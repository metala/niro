package tnk

import (
	"bytes"
	"errors"
	"io"
	"io/fs"
	"os/user"
	"strconv"
	"syscall"
	"time"

	"go.metala.org/niro/opis"
)

var (
	errHashLen = errors.New("wrong hash len")
)

type TreeEntry struct {
	Hash      *[32]byte
	HashFlags HashFlags
	Mode      fs.FileMode
	ModTime   time.Time
	Size      uint64
	Uid       uint32
	Gid       uint32
	Prefix    string
	FileName  string
	User      string
	Group     string
}

func (e *TreeEntry) SetHash(hash []byte) error {
	if len(hash) != 32 {
		return errHashLen
	}
	if e.Hash == nil {
		e.Hash = &[32]byte{}
	}
	copy(e.Hash[:], hash)
	return nil
}

func (e *TreeEntry) PopulateFrom(fi fs.FileInfo) {
	e.Mode = fi.Mode()
	e.ModTime = fi.ModTime()
	e.Size = uint64(fi.Size())
	e.FileName = fi.Name()
	systat := fi.Sys().(*syscall.Stat_t)
	if systat == nil {
		return
	}
	e.Uid = systat.Uid
	e.Gid = systat.Gid
	usr, err := user.LookupId(strconv.FormatInt(int64(e.Uid), 10))
	if err != nil {
		return
	}
	e.User = usr.Name
	grp, err := user.LookupGroupId(strconv.FormatInt(int64(e.Gid), 10))
	if err != nil {
		return
	}
	e.Group = grp.Name
}

func (e *TreeEntry) IsDir() bool {
	return e.Mode.IsDir()
}

func (e *TreeEntry) MarshalledLen() int {
	return 2 + 32 + 4 + 4 + 8 + 8 + 4 + 4 + 2 + len(e.Prefix) + 2 + len(e.FileName) + 2 + len(e.User) + 2 + len(e.Group)
}

func (e *TreeEntry) Marshal() []byte {
	b := make([]byte, 0, e.MarshalledLen())
	buf := bytes.NewBuffer(b)
	opis.WriteMany(buf,
		e.Hash[:], uint32(e.HashFlags), uint32(e.Mode),
		e.ModTime, e.Size, e.Uid, e.Gid,
		e.Prefix, e.FileName, e.User, e.Group,
	)
	return buf.Bytes()
}

func (e *TreeEntry) UnmarshalBytes(b []byte) error {
	r := bytes.NewBuffer(b)
	return e.Unmarshal(r)
}

func (e *TreeEntry) Unmarshal(r io.Reader) error {
	var hash []byte
	var hashflags uint32
	var mode uint32

	err := opis.ReadMany(r,
		&hash, &hashflags, &mode, &e.ModTime, &e.Size,
		&e.Uid, &e.Gid,
		&e.Prefix, &e.FileName, &e.User, &e.Group)
	if err != nil {
		return err
	}
	e.Mode = fs.FileMode(mode)
	e.HashFlags = HashFlags(hashflags)
	if len(hash) != 0 {
		e.SetHash(hash)
	}

	return nil
}
