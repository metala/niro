package tnk

import (
	"bytes"
	"crypto/cipher"
	"encoding/binary"
	"errors"
	"fmt"
	"io"

	"golang.org/x/crypto/chacha20poly1305"
	"golang.org/x/crypto/nacl/box"
)

const (
	messageSize = 16384
	flagsSize   = 1
)

var (
	errWrongNonce = errors.New("wrong nonce")

	zeroNonce [12]byte
)

func encryptionNonce(n uint64) (nonce [12]byte) {
	binary.BigEndian.PutUint64(nonce[4:12], n)
	return nonce
}

type EncryptionWriter struct {
	n       uint64
	w       io.WriteCloser
	buf     *bytes.Buffer
	written int64

	kdf *boundKdf
	dec cipher.AEAD
}

func NewEncryptionWriter(w io.WriteCloser, kdf *boundKdf) (*EncryptionWriter, error) {
	buf := bytes.NewBuffer(make([]byte, 0, messageSize))
	dk := kdf.DataKey()
	dec, _ := chacha20poly1305.New(dk[:])

	return &EncryptionWriter{
		w:   w,
		buf: buf,
		kdf: kdf,
		dec: dec,
	}, nil
}

func (e *EncryptionWriter) WriteHeader() (n int, err error) {
	epk, wrapk, err := e.kdf.WrapKey()
	if err != nil {
		return 0, err
	}
	c, _ := chacha20poly1305.New(wrapk[:])
	out := make([]byte, 32, 64+chacha20poly1305.Overhead)
	copy(out, epk)
	out = c.Seal(out, zeroNonce[:], e.kdf.contentKey[:], nil)
	return e.w.Write(out)
}

func (e *EncryptionWriter) WriteMetadata(fingerprint, flags []byte) (n int, err error) {
	mek := e.kdf.MetadataKey()
	mec, _ := chacha20poly1305.New(mek[:])
	nonce := zeroNonce
	out := make([]byte, 0, len(fingerprint)+chacha20poly1305.Overhead)
	out = mec.Seal(out[:0], nonce[:], fingerprint, nil)
	nb, err := e.w.Write(out)
	n += nb
	if err != nil {
		return n, err
	}

	out = make([]byte, 0, len(flags)+chacha20poly1305.Overhead)
	nonce = encryptionNonce(1)
	out = mec.Seal(out, nonce[:], flags, nil)
	nb, err = e.w.Write(out)
	n += nb
	if err != nil {
		return n, err
	}

	e.written += int64(n)
	e.kdf = nil // free
	return n, nil
}

func (e *EncryptionWriter) Write(p []byte) (n int, err error) {
	avail := messageSize - e.buf.Len()
	for len(p) > 0 {
		sz := len(p)
		if sz > avail {
			sz = avail
		}
		e.buf.Write(p[:sz])
		p = p[sz:]
		n += sz
		if avail -= sz; avail > 0 {
			continue
		}
		if err = e.FlushMessage(); err != nil {
			return n, err
		}
		avail = messageSize
	}

	n += len(p)
	return n, nil
}

func (e *EncryptionWriter) FlushMessage() error {
	msg := e.buf.Bytes()
	if len(msg) == 0 {
		return nil
	}

	nonce := encryptionNonce(e.n)
	out := make([]byte, 0, chacha20poly1305.Overhead+len(msg))
	out = e.dec.Seal(out, nonce[:], msg, nil)

	n, err := io.Copy(e.w, bytes.NewReader(out))
	if err != nil {
		return err
	}

	e.written += n
	e.buf.Reset()
	e.n += 1
	return nil
}

func (e *EncryptionWriter) Close() error {
	if err := e.FlushMessage(); err != nil {
		return err
	}
	return e.w.Close()
}

type EncryptionReader struct {
	n      uint64
	r      io.ReadCloser
	buf    *bytes.Buffer
	cipher cipher.AEAD

	Fingerprint []byte
	Flags       byte
}

func NewEncryptionReader(r io.ReadCloser, sk *[32]byte, kdf Kdf) (*EncryptionReader, error) {
	buf := bytes.NewBuffer(make([]byte, 0, messageSize+chacha20poly1305.Overhead))
	e := &EncryptionReader{r: r, buf: buf}

	var hdr [2*32 + chacha20poly1305.Overhead]byte
	if _, err := io.ReadFull(r, hdr[:]); err != nil {
		return nil, err
	}
	var epk, wrapk [32]byte
	copy(epk[:], hdr[:32])
	box.Precompute(&wrapk, &epk, sk)
	c, _ := chacha20poly1305.New(wrapk[:])
	// no error handling, len(epk) is always 32
	sealedContentKey := hdr[32:]
	contentKey, err := c.Open(nil, zeroNonce[:], sealedContentKey, nil)
	if err != nil {
		return nil, fmt.Errorf("open object ck: %s", err)
	}
	ck := ContentKey(contentKey)
	mek := kdf.MetadataKey(ck)
	if err := e.readMetadata(&mek); err != nil {
		return nil, fmt.Errorf("reading metadata: %w", err)
	}

	dek := kdf.DataKey(ck)
	e.cipher, _ = chacha20poly1305.New(dek[:])
	return e, nil
}

func (e *EncryptionReader) Read(b []byte) (int, error) {
	if e.buf.Len() == 0 {
		err := e.fetchNextDataMessage(messageSize)
		if err != nil && (err != io.EOF || e.buf.Len() == 0) {
			return 0, err
		}
	}

	n, err := e.buf.Read(b)
	if err == io.EOF {
		return n, nil
	}
	return n, err
}

func (e *EncryptionReader) readMetadata(mek *DerivedKey) error {
	if e.n != 0 {
		return errWrongNonce
	}
	fp := newFingerprint()
	ciphertext := make([]byte, fp.Size()+chacha20poly1305.Overhead)
	var err error
	if _, err = io.ReadFull(e.r, ciphertext); err != nil {
		return err
	}
	c, _ := chacha20poly1305.New(mek[:])
	var nonce [12]byte
	e.Fingerprint, err = c.Open(ciphertext[:0], nonce[:], ciphertext, nil)
	if err != nil {
		return err
	}

	ciphertext = make([]byte, flagsSize+chacha20poly1305.Overhead)
	if _, err = io.ReadFull(e.r, ciphertext); err != nil {
		return err
	}
	binary.BigEndian.PutUint64(nonce[4:12], 1)
	flags, err := c.Open(ciphertext[:0], nonce[:], ciphertext, nil)
	if err != nil {
		return err
	}
	e.Flags = flags[0]

	return err
}

func (e *EncryptionReader) fetchNextDataMessage(n int) error {
	e.buf.Reset()
	nr, err := io.CopyN(e.buf, e.r, chacha20poly1305.Overhead+int64(n))
	if err != nil && (err != io.EOF || nr == 0) {
		return err
	}

	b := e.buf.Bytes()
	nonce := encryptionNonce(e.n)
	if b, err = e.cipher.Open(b[:0], nonce[:], b, nil); err != nil {
		return err
	}

	e.buf = bytes.NewBuffer(b) // seek to start
	e.n += 1
	return nil
}

func (e *EncryptionReader) Close() error {
	e.buf = nil
	e.cipher = nil
	e.n = 0
	return e.r.Close()
}
