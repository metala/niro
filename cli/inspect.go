package main

import (
	"encoding/hex"
	"fmt"
	"io"
	"os"
	"strings"

	"github.com/spf13/afero"
	"github.com/spf13/cobra"
	"go.metala.org/niro"
	"go.metala.org/niro/tnk"
)

func init() {
	inspectCmd.Flags().StringVarP(&repositoryPath, "repository", "r", "", `/path/to/repository`)
	inspectCmd.MarkFlagRequired("repository")
	inspectCmd.Flags().BytesBase64VarP(&secretKey, "secret-key", "s", nil, `secret key in base64 format`)
	inspectCmd.MarkFlagRequired("secret-key")
	inspectCmd.Flags().StringVarP(&hash, "hash", "H", "", `object hash`)
	inspectCmd.MarkFlagRequired("hash")
	rootCmd.AddCommand(inspectCmd)
}

var inspectCmd = &cobra.Command{
	Use:   "inspect",
	Short: "Get information for an object hash",
	Long:  `Inspect command gives an information for a object withing the repository.`,
	Args:  cobra.ExactArgs(0),
	Run: func(cmd *cobra.Command, args []string) {
		repoFs := afero.NewBasePathFs(afero.NewOsFs(), repositoryPath)
		repo, err := niro.Existing(repoFs)
		if err != nil {
			fmt.Fprintf(os.Stderr, "failed to open repository: %s\n", err)
			os.Exit(2)
		}
		if err = repo.SetSecretKey(secretKey); err != nil {
			fmt.Fprintf(os.Stderr, "invalid secret key: %s\n", err)
			os.Exit(3)
		}
		h, err := hex.DecodeString(hash)
		if err != nil {
			fmt.Fprintf(os.Stderr, "invalid hash '%s': %s\n", hash, err)
			os.Exit(4)
		}

		info, r, err := repo.ReadObject(tnk.Hash(h))
		fmt.Printf("Object Size: %d bytes\n", info.ObjectSize)
		fmt.Printf("Fingerprint: %x\n", info.Fingerprint)
		fmt.Printf("Content Flags: %#v\n", info.ContentFlags)
		fmt.Printf("Compressed: %t\n", (info.ContentFlags&1 == 1))
		if err != nil {
			fmt.Fprintf(os.Stderr, "operation failed: %s\n", err)
			os.Exit(5)
		}
		defer r.Close()

		fmt.Println("\nData (xxd):")
		if err = xxd(r, os.Stdout); err != nil {
			fmt.Fprintf(os.Stderr, "operation failed: %s\n", err)
			os.Exit(5)
		}
	},
}

func xxd(r io.Reader, w io.Writer) error {
	base := 0
	for {
		xb := strings.Builder{}
		xb.WriteString(fmt.Sprintf("%08x: ", base))

		var b [16]byte
		n, err := io.ReadFull(r, b[:])
		if err != nil && err != io.EOF && err != io.ErrUnexpectedEOF {
			return err
		}

		for i := 0; i < n; i++ {
			xb.WriteString(fmt.Sprintf("%02x ", b[i]))
			if b[i] < 32 || b[i] >= 127 {
				b[i] = '.'
			}
		}
		for i := n; i < 16; i++ {
			xb.WriteString("   ")
		}

		xb.WriteByte(' ')
		xb.Write(b[:n])
		xb.WriteByte('\n')
		w.Write([]byte(xb.String()))
		if err == io.EOF || err == io.ErrUnexpectedEOF {
			break
		}
		base += 16
	}
	return nil
}
