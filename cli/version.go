package main

import (
	"fmt"

	"github.com/spf13/cobra"
	"go.metala.org/niro"
)

func init() {
	rootCmd.AddCommand(versionCmd)
}

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print the version number of Niro",
	Long:  `Print the semver version of Niro backup utility`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Printf("Niro: %s\n", niro.Version)
	},
}
