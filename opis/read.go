package opis

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io"
	"time"
)

func ReadMany(r io.Reader, args ...interface{}) error {
	for i, arg := range args {
		if err := Read(r, arg); err != nil {
			return fmt.Errorf("read arg #%d: %w", i, err)
		}
	}
	return nil
}

func Read(r io.Reader, out interface{}) error {
	switch v := out.(type) {
	case *[]byte:
		return ReadBytes(r, v)
	case *string:
		return ReadString(r, v)
	case *int16, *int32, *int64,
		*uint16, *uint32, *uint64:
		return binary.Read(r, binary.BigEndian, v)
	case *time.Time:
		return ReadTime(r, v)
	case *[32]byte:
		return Read32Bytes(r, v)
	}
	return fmt.Errorf("unsupported type '%T'", out)
}

func ReadString(r io.Reader, out *string) error {
	var b []byte
	if err := ReadBytes(r, &b); err != nil {
		return err
	}
	*out = string(b)
	return nil
}

func ReadBytes(r io.Reader, out *[]byte) error {
	var len uint16
	if err := binary.Read(r, binary.BigEndian, &len); err != nil {
		return fmt.Errorf("read len: %w", err)
	}
	buf := bytes.NewBuffer(nil)
	if _, err := io.CopyN(buf, r, int64(len)); err != nil {
		return fmt.Errorf("read bytes: %w", err)
	}
	*out = buf.Bytes()

	return nil
}

func Read32Bytes(r io.Reader, out *[32]byte) error {
	var buf [32]byte
	if _, err := io.ReadFull(r, buf[:]); err != nil {
		return fmt.Errorf("read 32 bytes: %w", err)
	}
	*out = buf
	return nil
}

func ReadTime(r io.Reader, out *time.Time) error {
	var ts int64
	if err := Read(r, &ts); err != nil {
		return fmt.Errorf("read time: %w", err)
	}
	*out = time.UnixMilli(ts).UTC()
	return nil
}
