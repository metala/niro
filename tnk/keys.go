package tnk

import (
	"hash"

	"github.com/zeebo/blake3"
	"golang.org/x/crypto/curve25519"
	"golang.org/x/crypto/nacl/box"
)

type PublicKey = [32]byte
type SecretKey = [32]byte

type DerivedKey = [32]byte
type WrapPublicKey = []byte

type Kdf interface {
	ContentKeyMaker() hash.Hash
	Hash(ck ContentKey) Hash
	MetadataKey(ck ContentKey) (mek DerivedKey)
	DataKey(ck ContentKey) (dek DerivedKey)
	WrapKey(ck ContentKey, pk *PublicKey) (wpk WrapPublicKey, wrapk DerivedKey, err error)
}

type tnkKdf struct {
	content     string
	hash        string
	dataKey     string
	metadataKey string
	kexKey      string
}

type ContentKey []byte

const (
	contentKeyKdfSuffix  = " content key v1"
	hashKdfSuffix        = " hash v1"
	metadataKeyKdfSuffix = " metadata key v1"
	dataKeyKdfSuffix     = " data key v1"
	kexKeyKdfSuffix      = " kex key v1"
)

func NewKdf(prefix string) *tnkKdf {
	return &tnkKdf{
		content:     prefix + contentKeyKdfSuffix,
		hash:        prefix + hashKdfSuffix,
		dataKey:     prefix + dataKeyKdfSuffix,
		metadataKey: prefix + metadataKeyKdfSuffix,
		kexKey:      prefix + kexKeyKdfSuffix,
	}
}

func (k *tnkKdf) ContentKeyMaker() hash.Hash {
	return blake3.NewDeriveKey(k.content)
}

func (k *tnkKdf) Hash(ck ContentKey) Hash {
	h := make([]byte, 32)
	blake3.DeriveKey(k.hash, ck, h)
	return h
}

func (k *tnkKdf) MetadataKey(ck ContentKey) (mek DerivedKey) {
	blake3.DeriveKey(k.metadataKey, ck, mek[:])
	return mek
}

func (k *tnkKdf) DataKey(ck ContentKey) (dek DerivedKey) {
	blake3.DeriveKey(k.dataKey, ck, dek[:])
	return dek
}

func (k *tnkKdf) WrapKey(ck ContentKey, pk *PublicKey) (wpk WrapPublicKey, wrapk DerivedKey, err error) {
	var esk DerivedKey
	blake3.DeriveKey(k.kexKey, ck, esk[:])
	wpk, err = curve25519.X25519(esk[:], curve25519.Basepoint)
	if err != nil {
		return
	}

	box.Precompute(&wrapk, pk, &esk)
	return
}

type boundKdf struct {
	kdf        Kdf
	contentKey ContentKey
	publicKey  *PublicKey
}

func (k *boundKdf) Hash() Hash {
	return k.kdf.Hash(k.contentKey)
}

func (k *boundKdf) MetadataKey() (mek DerivedKey) {
	return k.kdf.MetadataKey(k.contentKey)
}

func (k *boundKdf) DataKey() (dek DerivedKey) {
	return k.kdf.DataKey(k.contentKey)
}

func (k *boundKdf) WrapKey() (wpk WrapPublicKey, wrapk DerivedKey, err error) {
	return k.kdf.WrapKey(k.contentKey, k.publicKey)
}
