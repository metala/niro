module go.metala.org/niro

go 1.19

require (
	github.com/docker/go-units v0.5.0
	github.com/hanwen/go-fuse/v2 v2.5.1
	github.com/jotfs/fastcdc-go v0.2.0
	github.com/klauspost/compress v1.17.8
	github.com/klauspost/readahead v1.4.0
	github.com/matryer/is v1.4.1
	github.com/pelletier/go-toml/v2 v2.2.1
	github.com/spf13/afero v1.11.0
	github.com/spf13/cobra v1.8.0
	github.com/zeebo/blake3 v0.2.3
	golang.org/x/crypto v0.22.0
)

require (
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/klauspost/cpuid/v2 v2.2.7 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/sys v0.19.0 // indirect
	golang.org/x/text v0.14.0 // indirect
)
