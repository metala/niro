package niro

import (
	"archive/tar"
	"bytes"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"log"
	"path"
	"time"

	"go.metala.org/niro/tnk"

	"github.com/docker/go-units"
)

func (n *Niro) TarImport(rd io.Reader, name string) (*tnk.Archive, tnk.Hash, error) {
	var deduped, written, total, fileCount, dirCount, linkCount uint64

	start := time.Now()
	trd := tar.NewReader(rd)
	er, ew := io.Pipe()
	resultc, errc := n.WriteFileAsync(er)
	ew.Write(tnk.TreeMagic)
	for {
		record, err := trd.Next()
		if err == io.EOF {
			break
		} else if err != nil {
			return nil, nil, fmt.Errorf("read tar record: %w", err)
		}

		prefix, baseName := path.Split(path.Clean(record.Name))
		entry := tnk.TreeEntry{
			Prefix:   prefix,
			FileName: baseName,
			Mode:     fs.FileMode(record.Mode),
			ModTime:  record.ModTime,
			Size:     uint64(record.Size),
			Uid:      uint32(record.Uid),
			Gid:      uint32(record.Gid),
			User:     record.Uname,
			Group:    record.Gname,
		}

		if record.Typeflag == tar.TypeDir {
			if n.Verbosity >= 1 {
				log.Printf("dir   %s %d/%d (%s/%s) %-10s %s %s", entry.Mode, entry.Uid, entry.Gid, entry.User, entry.Group, units.BytesSize(float64(entry.Size)), entry.ModTime.Format(time.DateTime), record.Name)
			}
			dirCount += 1
			entry.Mode |= fs.ModeDir
			entry.SetHash(tnk.ZeroHash[:])
		} else if record.Typeflag == tar.TypeReg || record.Typeflag == tar.TypeXGlobalHeader {
			if n.Verbosity >= 1 {
				log.Printf("file  %s %d/%d (%s/%s) %-10s %s %s", entry.Mode, entry.Uid, entry.Gid, entry.User, entry.Group, units.BytesSize(float64(entry.Size)), entry.ModTime.Format(time.DateTime), record.Name)
			}
			result, err := n.WriteFile(trd)
			if err != nil {
				return nil, nil, fmt.Errorf("write file: %w", err)
			}
			total += entry.Size
			written += result.Written
			deduped += result.Deduped
			fileCount += 1
			entry.SetHash(result.Hash)
			if result.HashFlags.IsSet(tnk.FlagComposite) {
				entry.HashFlags |= tnk.FlagComposite
			}
		} else if record.Typeflag == tar.TypeSymlink || record.Typeflag == tar.TypeLink {
			if n.Verbosity >= 1 {
				log.Printf("link  %s %d/%d (%s/%s) %-10s %s %s -> %s", entry.Mode, entry.Uid, entry.Gid, entry.User, entry.Group, units.BytesSize(float64(entry.Size)), entry.ModTime.Format(time.DateTime), record.Name, record.Linkname)
			}
			result, err := n.WriteObjectData(tnk.DataType, []byte(record.Linkname))
			if err != nil {
				return nil, nil, fmt.Errorf("write link: %w", err)
			}
			linkCount += 1
			entry.SetHash(result.Hash)
			entry.Mode |= fs.ModeSymlink
		} else {
			return nil, nil, fmt.Errorf("invalid entry: %#v", record)
		}

		data := entry.Marshal()
		if _, err := ew.Write(data); err != nil {
			return nil, nil, fmt.Errorf("write tree entry: %w", err)
		}

		select {
		case err := <-errc:
			return nil, nil, fmt.Errorf("early tree fail: %w", err)
		default:
		}
	}

	ew.Close()
	if err := <-errc; err != nil {
		return nil, nil, fmt.Errorf("tree fail: %w", err)
	}
	result := <-resultc
	if n.Verbosity >= 1 {
		now := time.Now()
		duration := now.Sub(start)
		fmt.Println("---------------------------------")
		fmt.Printf("Start:    %s\n", start)
		fmt.Printf("End:      %s\n", now)
		fmt.Printf("Duration: %s\n", duration)

		fmt.Println()
		ratio := uint64(0)
		if total != 0 {
			ratio = (written * 100) / total
		}
		fmt.Printf("Counts (file/dir/link):  %d / %d / %d\n", fileCount, dirCount, linkCount)
		fmt.Printf("Total Bytes:             %s\n", units.BytesSize(float64(total)))
		fmt.Printf("Written Bytes:           %s\n", units.BytesSize(float64(written)))
		fmt.Printf("Deduped Bytes:           %s\n", units.BytesSize(float64(deduped)))
		fmt.Printf("Ratio (written / total): %d%%\n", ratio)
		fmt.Println("---------------------------------")
	}
	ar := tnk.Archive{
		Timestamp: time.Now().UTC(),
		Hash:      result.Hash,
		HashFlags: result.HashFlags,
		Name:      name,
	}
	stored, err := n.WriteObjectData(tnk.ArchiveType, ar.Marshal())
	if err != nil {
		return nil, nil, fmt.Errorf("write archive: %w", err)
	}
	return &ar, stored.Hash, nil
}

func (n *Niro) TarExport(wr io.WriteCloser, archiveHash tnk.Hash) error {
	twr := tar.NewWriter(wr)
	ar, err := n.ReadArchive(archiveHash)
	if err != nil {
		return err
	}
	atrd, err := n.ReadFile(ar.Hash, ar.HashFlags.IsSet(tnk.FlagComposite))
	if err != nil {
		return err
	}
	defer atrd.Close()
	if isTree, err := tnk.CheckMagic(atrd, tnk.TreeMagic); err != nil {
		return err
	} else if !isTree {
		return errors.New("not a tree")
	}

	for {
		entry := tnk.TreeEntry{}
		if err := entry.Unmarshal(atrd); errors.Is(err, io.EOF) {
			break
		} else if err != nil {
			return err
		}
		if err := n.tarWriteEntry(twr, &entry); err != nil {
			return err
		}
	}
	return twr.Close()
}

func (n *Niro) tarWriteEntry(w *tar.Writer, entry *tnk.TreeEntry) error {
	hdr := tar.Header{
		Typeflag: tar.TypeReg,
		Name:     path.Join(entry.Prefix, entry.FileName),
		Mode:     int64(entry.Mode),
		Size:     int64(entry.Size),
		Uid:      int(entry.Uid),
		Gid:      int(entry.Gid),
		Uname:    entry.User,
		Gname:    entry.Group,
		ModTime:  entry.ModTime,
	}
	if entry.IsDir() || bytes.Equal(tnk.ZeroHash[:], entry.Hash[:]) {
		hdr.Typeflag = tar.TypeDir
		hdr.Name += "/"
	} else if entry.Mode&fs.ModeSymlink != 0 {
		_, r, err := n.ReadObject(entry.Hash[:])
		if err != nil {
			return err
		}
		defer r.Close()
		linkname, err := io.ReadAll(r)
		if err != nil {
			return err
		}
		hdr.Linkname = string(linkname)
		hdr.Size = 0
	}

	if err := w.WriteHeader(&hdr); err != nil {
		return err
	}
	if hdr.Size == 0 {
		return nil
	}
	frd, err := n.ReadFile(entry.Hash[:], entry.HashFlags.IsSet(tnk.FlagComposite))
	if err != nil {
		return err
	}
	defer frd.Close()
	if _, err := io.Copy(w, frd); err != nil {
		return err
	}

	return nil
}
