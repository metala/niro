# Niro Backup
Deduplicating backup tool inspired by Borg backup.

**Features**
* Uses FastCDC chunker and Zstd compression of the chunks.
* Uses public-key cryptography (X25519 + Cacha20poly1305).
* Uses BLAKE3 for salted object hash generation and as a KDF for the convergent encryption.
* The secret key is not required, except when you want to restore a backup.
* If the public key is not available, you should not be able to check if a content is present on the repository.
* Full backups. No diffing. Incrementally adds new / changed content.
* Does not require server counterpart executable.
* Suitable for immutable storage - e.g. S3 WORM (not implemented yet).
* Supports FUSE mount of an archive.

Copyright Licence: 2-Clause BSD Licence

# Disclaimer
This is experimental tool. It is very likely that data loss will occur.
The compression rate is worse than a standard tarball with Zstd. The part this tool shows it's edge is when you have repeated files or parts of files that would be deduplicated.

# Todos
 - [ ] Ensure it is stable and well-tested
 - [ ] S3, SSH, FTP support
 - [ ] Improve performance and concurency
 - [ ] OpenBSD fusefs support
 - [ ] Delete older archives and objects


# Usage
```
$ niro help
Niro is backup utility that features deduplication and asymmetric encryption

Usage:
  niro [flags]
  niro [command]

Available Commands:
  completion  Generate the autocompletion script for the specified shell
  help        Help about any command
  init        Initializes backup repository
  inspect     Get information for an object hash
  keygen      Generates a Curve25519 key-pair
  list        List archives
  mount       Mount archive at filesystem
  tar         Tar archive operations
  version     Print the version number of Niro
```

## Keygen
Generates a X25519 key-pair, or just the public key, if secret key is provided, for the assymetic encryption part.
```
$ niro help keygen
Usage:
  niro keygen [flags]

Flags:
  -s, --secret-key bytesBase64   secret key in base64 format
```

```
$ niro keygen
Secret Key (sk): KEXr3kP/GY0pksw/51nppYDB1c2qtUhpoiWhzgZ2Wos=
Public Key (pk): 7szz31pqLTVay6W2lWZro7PzK23Tlv9JLoM1/7ZBdw8=
```

## Initialisation of a repository
Init create a repositry configuration and seeds it with random salt.

```
$ niro help init
Usage:
  niro init [flags]

Flags:
  -r, --repository string   /path/to/repository
```

```
$ niro init -r ./repo
$ $ ls -asl ./repo
total 16
4 drwxr-xr-x  3 user user 4096 Jan 11 15:24 .
4 drwx------ 43 user user 4096 Jan 11 15:24 ..
4 -rw-r--r--  1 user user   65 Jan 11 15:24 config
4 drwxr-xr-x  2 user user 4096 Jan 11 15:24 objects
```

## Import tar archive
```
$ niro help tar import
Usage:
  niro tar import [flags]

Flags:
  -c, --concurrency uint         write / compression concurrency (default 1)
  -i, --input string             input file (use "-" for stdin)
  -n, --name string              archive name
  -p, --public-key bytesBase64   public key in base64
  -r, --repository string        /path/to/repository

required flag(s) "input", "name", "public-key", "repository"
```

### Example
Command: `niro tar import -v -r ./repo -i archive.tar -n 'some name' -p '<public key>'`

<details>
  <summary>qbe-1.0.tar</summary>
<pre>
$ ls -l qbe-1.0.tar
-rw-r--r-- 1 user user 1617920 Jun 22  2022 qbe-1.0.tar
$ niro tar import -v -r ./repo -i qbe-1.0.tar -n 'qbe-1.0.tar' -p '7szz31pqLTVay6W2lWZro7PzK23Tlv9JLoM1/7ZBdw8='
2025/01/11 16:34:43 file  ---------- 0/0 (/) 0B         0001-01-01 00:00:00 pax_global_header
2025/01/11 16:34:43 dir   -rwxrwxr-x 0/0 (root/root) 0B         2022-06-16 10:49:09 qbe-1.0/
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 32B        2022-06-16 10:49:09 qbe-1.0/.gitignore
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 1.05KiB    2022-06-16 10:49:09 qbe-1.0/LICENSE
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 2.297KiB   2022-06-16 10:49:09 qbe-1.0/Makefile
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 536B       2022-06-16 10:49:09 qbe-1.0/README
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 3.195KiB   2022-06-16 10:49:09 qbe-1.0/alias.c
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 8.507KiB   2022-06-16 10:49:09 qbe-1.0/all.h
2025/01/11 16:34:43 dir   -rwxrwxr-x 0/0 (root/root) 0B         2022-06-16 10:49:09 qbe-1.0/amd64/
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 903B       2022-06-16 10:49:09 qbe-1.0/amd64/all.h
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 13.52KiB   2022-06-16 10:49:09 qbe-1.0/amd64/emit.c
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 14.82KiB   2022-06-16 10:49:09 qbe-1.0/amd64/isel.c
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 14.63KiB   2022-06-16 10:49:09 qbe-1.0/amd64/sysv.c
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 571B       2022-06-16 10:49:09 qbe-1.0/amd64/targ.c
2025/01/11 16:34:43 dir   -rwxrwxr-x 0/0 (root/root) 0B         2022-06-16 10:49:09 qbe-1.0/arm64/
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 13.56KiB   2022-06-16 10:49:09 qbe-1.0/arm64/abi.c
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 873B       2022-06-16 10:49:09 qbe-1.0/arm64/all.h
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 11.76KiB   2022-06-16 10:49:09 qbe-1.0/arm64/emit.c
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 4.69KiB    2022-06-16 10:49:09 qbe-1.0/arm64/isel.c
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 1015B      2022-06-16 10:49:09 qbe-1.0/arm64/targ.c
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 4.966KiB   2022-06-16 10:49:09 qbe-1.0/cfg.c
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 4.116KiB   2022-06-16 10:49:09 qbe-1.0/copy.c
2025/01/11 16:34:43 dir   -rwxrwxr-x 0/0 (root/root) 0B         2022-06-16 10:49:09 qbe-1.0/doc/
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 4.972KiB   2022-06-16 10:49:09 qbe-1.0/doc/abi.txt
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 32.63KiB   2022-06-16 10:49:09 qbe-1.0/doc/il.txt
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 3.337KiB   2022-06-16 10:49:09 qbe-1.0/doc/llvm.txt
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 537B       2022-06-16 10:49:09 qbe-1.0/doc/rv64.txt
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 647B       2022-06-16 10:49:09 qbe-1.0/doc/win.txt
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 10.98KiB   2022-06-16 10:49:09 qbe-1.0/fold.c
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 2.969KiB   2022-06-16 10:49:09 qbe-1.0/gas.c
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 3.031KiB   2022-06-16 10:49:09 qbe-1.0/live.c
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 8.711KiB   2022-06-16 10:49:09 qbe-1.0/load.c
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 3.575KiB   2022-06-16 10:49:09 qbe-1.0/main.c
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 1.781KiB   2022-06-16 10:49:09 qbe-1.0/mem.c
2025/01/11 16:34:43 dir   -rwxrwxr-x 0/0 (root/root) 0B         2022-06-16 10:49:09 qbe-1.0/minic/
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 21B        2022-06-16 10:49:09 qbe-1.0/minic/.gitignore
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 146B       2022-06-16 10:49:09 qbe-1.0/minic/Makefile
2025/01/11 16:34:43 file  -rwxrwxr-x 0/0 (root/root) 470B       2022-06-16 10:49:09 qbe-1.0/minic/mcc
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 15.45KiB   2022-06-16 10:49:09 qbe-1.0/minic/minic.y
2025/01/11 16:34:43 dir   -rwxrwxr-x 0/0 (root/root) 0B         2022-06-16 10:49:09 qbe-1.0/minic/test/
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 401B       2022-06-16 10:49:09 qbe-1.0/minic/test/collatz.c
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 340B       2022-06-16 10:49:09 qbe-1.0/minic/test/euler9.c
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 776B       2022-06-16 10:49:09 qbe-1.0/minic/test/knight.c
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 1.311KiB   2022-06-16 10:49:09 qbe-1.0/minic/test/mandel.c
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 294B       2022-06-16 10:49:09 qbe-1.0/minic/test/prime.c
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 921B       2022-06-16 10:49:09 qbe-1.0/minic/test/queen.c
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 22.79KiB   2022-06-16 10:49:09 qbe-1.0/minic/yacc.c
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 7.102KiB   2022-06-16 10:49:09 qbe-1.0/ops.h
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 22.54KiB   2022-06-16 10:49:09 qbe-1.0/parse.c
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 13.9KiB    2022-06-16 10:49:09 qbe-1.0/rega.c
2025/01/11 16:34:43 dir   -rwxrwxr-x 0/0 (root/root) 0B         2022-06-16 10:49:09 qbe-1.0/rv64/
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 12.14KiB   2022-06-16 10:49:09 qbe-1.0/rv64/abi.c
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 994B       2022-06-16 10:49:09 qbe-1.0/rv64/all.h
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 11.16KiB   2022-06-16 10:49:09 qbe-1.0/rv64/emit.c
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 5.426KiB   2022-06-16 10:49:09 qbe-1.0/rv64/isel.c
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 1.026KiB   2022-06-16 10:49:09 qbe-1.0/rv64/targ.c
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 10.42KiB   2022-06-16 10:49:09 qbe-1.0/spill.c
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 7.588KiB   2022-06-16 10:49:09 qbe-1.0/ssa.c
2025/01/11 16:34:43 dir   -rwxrwxr-x 0/0 (root/root) 0B         2022-06-16 10:49:09 qbe-1.0/test/
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 529B       2022-06-16 10:49:09 qbe-1.0/test/_alt.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 51.94KiB   2022-06-16 10:49:09 qbe-1.0/test/_bf99.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 154KiB     2022-06-16 10:49:09 qbe-1.0/test/_bfmandel.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 5.425KiB   2022-06-16 10:49:09 qbe-1.0/test/_chacha20.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 403B       2022-06-16 10:49:09 qbe-1.0/test/_dragon.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 155B       2022-06-16 10:49:09 qbe-1.0/test/_fix1.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 166B       2022-06-16 10:49:09 qbe-1.0/test/_fix2.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 225B       2022-06-16 10:49:09 qbe-1.0/test/_fix3.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 353B       2022-06-16 10:49:09 qbe-1.0/test/_fix4.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 351B       2022-06-16 10:49:09 qbe-1.0/test/_live.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 111B       2022-06-16 10:49:09 qbe-1.0/test/_rpo.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 851.4KiB   2022-06-16 10:49:09 qbe-1.0/test/_slow.qbe
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 335B       2022-06-16 10:49:09 qbe-1.0/test/_spill1.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 410B       2022-06-16 10:49:09 qbe-1.0/test/_spill2.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 368B       2022-06-16 10:49:09 qbe-1.0/test/_spill3.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 1.228KiB   2022-06-16 10:49:09 qbe-1.0/test/abi1.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 413B       2022-06-16 10:49:09 qbe-1.0/test/abi2.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 870B       2022-06-16 10:49:09 qbe-1.0/test/abi3.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 612B       2022-06-16 10:49:09 qbe-1.0/test/abi4.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 3.414KiB   2022-06-16 10:49:09 qbe-1.0/test/abi5.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 845B       2022-06-16 10:49:09 qbe-1.0/test/abi6.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 361B       2022-06-16 10:49:09 qbe-1.0/test/abi7.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 7.131KiB   2022-06-16 10:49:09 qbe-1.0/test/abi8.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 248B       2022-06-16 10:49:09 qbe-1.0/test/align.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 244B       2022-06-16 10:49:09 qbe-1.0/test/cmp1.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 1.226KiB   2022-06-16 10:49:09 qbe-1.0/test/collatz.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 1.17KiB    2022-06-16 10:49:09 qbe-1.0/test/conaddr.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 1.854KiB   2022-06-16 10:49:09 qbe-1.0/test/cprime.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 269B       2022-06-16 10:49:09 qbe-1.0/test/cup.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 621B       2022-06-16 10:49:09 qbe-1.0/test/dark.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 407B       2022-06-16 10:49:09 qbe-1.0/test/double.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 339B       2022-06-16 10:49:09 qbe-1.0/test/dynalloc.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 574B       2022-06-16 10:49:09 qbe-1.0/test/echo.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 386B       2022-06-16 10:49:09 qbe-1.0/test/env.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 375B       2022-06-16 10:49:09 qbe-1.0/test/eucl.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 425B       2022-06-16 10:49:09 qbe-1.0/test/euclc.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 242B       2022-06-16 10:49:09 qbe-1.0/test/fixarg.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 682B       2022-06-16 10:49:09 qbe-1.0/test/fold1.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 2.407KiB   2022-06-16 10:49:09 qbe-1.0/test/fpcnv.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 535B       2022-06-16 10:49:09 qbe-1.0/test/isel1.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 2.364KiB   2022-06-16 10:49:09 qbe-1.0/test/isel2.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 1.649KiB   2022-06-16 10:49:09 qbe-1.0/test/isel3.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 637B       2022-06-16 10:49:09 qbe-1.0/test/ldbits.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 288B       2022-06-16 10:49:09 qbe-1.0/test/ldhoist.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 473B       2022-06-16 10:49:09 qbe-1.0/test/load1.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 357B       2022-06-16 10:49:09 qbe-1.0/test/loop.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 5.203KiB   2022-06-16 10:49:09 qbe-1.0/test/mandel.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 574B       2022-06-16 10:49:09 qbe-1.0/test/max.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 566B       2022-06-16 10:49:09 qbe-1.0/test/philv.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 552B       2022-06-16 10:49:09 qbe-1.0/test/prime.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 312B       2022-06-16 10:49:09 qbe-1.0/test/puts10.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 5.193KiB   2022-06-16 10:49:09 qbe-1.0/test/queen.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 457B       2022-06-16 10:49:09 qbe-1.0/test/rega1.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 1.376KiB   2022-06-16 10:49:09 qbe-1.0/test/spill1.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 1.146KiB   2022-06-16 10:49:09 qbe-1.0/test/strcmp.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 1.316KiB   2022-06-16 10:49:09 qbe-1.0/test/strspn.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 608B       2022-06-16 10:49:09 qbe-1.0/test/sum.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 547B       2022-06-16 10:49:09 qbe-1.0/test/vararg1.ssa
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 18.37KiB   2022-06-16 10:49:09 qbe-1.0/test/vararg2.ssa
2025/01/11 16:34:43 dir   -rwxrwxr-x 0/0 (root/root) 0B         2022-06-16 10:49:09 qbe-1.0/tools/
2025/01/11 16:34:43 file  -rwxrwxr-x 0/0 (root/root) 2.866KiB   2022-06-16 10:49:09 qbe-1.0/tools/abi8.py
2025/01/11 16:34:43 file  -rwxrwxr-x 0/0 (root/root) 1.28KiB    2022-06-16 10:49:09 qbe-1.0/tools/abifuzz.sh
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 13.34KiB   2022-06-16 10:49:09 qbe-1.0/tools/callgen.ml
2025/01/11 16:34:43 file  -rwxrwxr-x 0/0 (root/root) 503B       2022-06-16 10:49:09 qbe-1.0/tools/cra.sh
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 1.887KiB   2022-06-16 10:49:09 qbe-1.0/tools/lexh.c
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 4.86KiB    2022-06-16 10:49:09 qbe-1.0/tools/pmov.c
2025/01/11 16:34:43 file  -rwxrwxr-x 0/0 (root/root) 2.934KiB   2022-06-16 10:49:09 qbe-1.0/tools/test.sh
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 2.909KiB   2022-06-16 10:49:09 qbe-1.0/tools/vatest.py
2025/01/11 16:34:43 file  -rw-rw-r-- 0/0 (root/root) 9.482KiB   2022-06-16 10:49:09 qbe-1.0/util.c
---------------------------------
Start:    2025-01-11 16:34:43.674253281 +0200 EET m=+0.003501275
End:      2025-01-11 16:34:43.839538482 +0200 EET m=+0.168786399
Duration: 165.285124ms

Counts (file/dir/link):  119 / 9 / 0
Total Bytes:             1.449MiB
Written Bytes:           393.1KiB
Deduped Bytes:           0B
Ratio (written / total): 26%
---------------------------------
Created archive with hash: acb0ef5cc7834f5f96d6081d0d45f3a5a1ed0bb2dc29d702e3078d848269f833

$ du -sh --apparent-size repo/
833K	repo/
</pre>
</details>

<details>
  <summary>qbe-1.1.tar</summary>
<pre>
$ ls -l qbe-1.1.tar
-rw-r--r-- 1 user user 1658880 Feb  1  2023 qbe-1.1.tar
$ niro tar import -v -r ./repo -i qbe-1.1.tar -n 'qbe-1.1.tar' -p '7szz31pqLTVay6W2lWZro7PzK23Tlv9JLoM1/7ZBdw8='
2025/01/11 16:36:30 file  ---------- 0/0 (/) 0B         0001-01-01 00:00:00 pax_global_header
2025/01/11 16:36:30 dir   -rwxrwxr-x 0/0 (root/root) 0B         2023-01-09 15:05:06 qbe-1.1/
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 32B        2023-01-09 15:05:06 qbe-1.1/.gitignore
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 1.05KiB    2023-01-09 15:05:06 qbe-1.1/LICENSE
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 2.39KiB    2023-01-09 15:05:06 qbe-1.1/Makefile
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 536B       2023-01-09 15:05:06 qbe-1.1/README
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 407B       2023-01-09 15:05:06 qbe-1.1/abi.c
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 4.253KiB   2023-01-09 15:05:06 qbe-1.1/alias.c
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 9.133KiB   2023-01-09 15:05:06 qbe-1.1/all.h
2025/01/11 16:36:30 dir   -rwxrwxr-x 0/0 (root/root) 0B         2023-01-09 15:05:06 qbe-1.1/amd64/
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 903B       2023-01-09 15:05:06 qbe-1.1/amd64/all.h
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 13.7KiB    2023-01-09 15:05:06 qbe-1.1/amd64/emit.c
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 15.44KiB   2023-01-09 15:05:06 qbe-1.1/amd64/isel.c
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 14.69KiB   2023-01-09 15:05:06 qbe-1.1/amd64/sysv.c
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 835B       2023-01-09 15:05:06 qbe-1.1/amd64/targ.c
2025/01/11 16:36:30 dir   -rwxrwxr-x 0/0 (root/root) 0B         2023-01-09 15:05:06 qbe-1.1/arm64/
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 16.27KiB   2023-01-09 15:05:06 qbe-1.1/arm64/abi.c
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 897B       2023-01-09 15:05:06 qbe-1.1/arm64/all.h
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 12.85KiB   2023-01-09 15:05:06 qbe-1.1/arm64/emit.c
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 5.551KiB   2023-01-09 15:05:06 qbe-1.1/arm64/isel.c
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 1.269KiB   2023-01-09 15:05:06 qbe-1.1/arm64/targ.c
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 4.966KiB   2023-01-09 15:05:06 qbe-1.1/cfg.c
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 4.403KiB   2023-01-09 15:05:06 qbe-1.1/copy.c
2025/01/11 16:36:30 dir   -rwxrwxr-x 0/0 (root/root) 0B         2023-01-09 15:05:06 qbe-1.1/doc/
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 4.972KiB   2023-01-09 15:05:06 qbe-1.1/doc/abi.txt
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 35.72KiB   2023-01-09 15:05:06 qbe-1.1/doc/il.txt
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 3.337KiB   2023-01-09 15:05:06 qbe-1.1/doc/llvm.txt
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 537B       2023-01-09 15:05:06 qbe-1.1/doc/rv64.txt
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 647B       2023-01-09 15:05:06 qbe-1.1/doc/win.txt
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 3.749KiB   2023-01-09 15:05:06 qbe-1.1/emit.c
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 11.07KiB   2023-01-09 15:05:06 qbe-1.1/fold.c
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 3.031KiB   2023-01-09 15:05:06 qbe-1.1/live.c
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 9.371KiB   2023-01-09 15:05:06 qbe-1.1/load.c
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 3.418KiB   2023-01-09 15:05:06 qbe-1.1/main.c
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 8.451KiB   2023-01-09 15:05:06 qbe-1.1/mem.c
2025/01/11 16:36:30 dir   -rwxrwxr-x 0/0 (root/root) 0B         2023-01-09 15:05:06 qbe-1.1/minic/
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 21B        2023-01-09 15:05:06 qbe-1.1/minic/.gitignore
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 146B       2023-01-09 15:05:06 qbe-1.1/minic/Makefile
2025/01/11 16:36:30 file  -rwxrwxr-x 0/0 (root/root) 470B       2023-01-09 15:05:06 qbe-1.1/minic/mcc
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 15.45KiB   2023-01-09 15:05:06 qbe-1.1/minic/minic.y
2025/01/11 16:36:30 dir   -rwxrwxr-x 0/0 (root/root) 0B         2023-01-09 15:05:06 qbe-1.1/minic/test/
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 401B       2023-01-09 15:05:06 qbe-1.1/minic/test/collatz.c
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 340B       2023-01-09 15:05:06 qbe-1.1/minic/test/euler9.c
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 776B       2023-01-09 15:05:06 qbe-1.1/minic/test/knight.c
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 1.311KiB   2023-01-09 15:05:06 qbe-1.1/minic/test/mandel.c
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 294B       2023-01-09 15:05:06 qbe-1.1/minic/test/prime.c
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 921B       2023-01-09 15:05:06 qbe-1.1/minic/test/queen.c
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 22.79KiB   2023-01-09 15:05:06 qbe-1.1/minic/yacc.c
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 7.6KiB     2023-01-09 15:05:06 qbe-1.1/ops.h
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 24.06KiB   2023-01-09 15:05:06 qbe-1.1/parse.c
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 13.9KiB    2023-01-09 15:05:06 qbe-1.1/rega.c
2025/01/11 16:36:30 dir   -rwxrwxr-x 0/0 (root/root) 0B         2023-01-09 15:05:06 qbe-1.1/rv64/
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 12.58KiB   2023-01-09 15:05:06 qbe-1.1/rv64/abi.c
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 994B       2023-01-09 15:05:06 qbe-1.1/rv64/all.h
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 11.74KiB   2023-01-09 15:05:06 qbe-1.1/rv64/emit.c
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 5.431KiB   2023-01-09 15:05:06 qbe-1.1/rv64/isel.c
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 1.084KiB   2023-01-09 15:05:06 qbe-1.1/rv64/targ.c
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 1.429KiB   2023-01-09 15:05:06 qbe-1.1/simpl.c
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 10.42KiB   2023-01-09 15:05:06 qbe-1.1/spill.c
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 7.681KiB   2023-01-09 15:05:06 qbe-1.1/ssa.c
2025/01/11 16:36:30 dir   -rwxrwxr-x 0/0 (root/root) 0B         2023-01-09 15:05:06 qbe-1.1/test/
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 529B       2023-01-09 15:05:06 qbe-1.1/test/_alt.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 51.94KiB   2023-01-09 15:05:06 qbe-1.1/test/_bf99.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 154KiB     2023-01-09 15:05:06 qbe-1.1/test/_bfmandel.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 5.425KiB   2023-01-09 15:05:06 qbe-1.1/test/_chacha20.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 403B       2023-01-09 15:05:06 qbe-1.1/test/_dragon.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 155B       2023-01-09 15:05:06 qbe-1.1/test/_fix1.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 166B       2023-01-09 15:05:06 qbe-1.1/test/_fix2.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 225B       2023-01-09 15:05:06 qbe-1.1/test/_fix3.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 353B       2023-01-09 15:05:06 qbe-1.1/test/_fix4.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 351B       2023-01-09 15:05:06 qbe-1.1/test/_live.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 111B       2023-01-09 15:05:06 qbe-1.1/test/_rpo.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 851.4KiB   2023-01-09 15:05:06 qbe-1.1/test/_slow.qbe
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 335B       2023-01-09 15:05:06 qbe-1.1/test/_spill1.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 410B       2023-01-09 15:05:06 qbe-1.1/test/_spill2.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 368B       2023-01-09 15:05:06 qbe-1.1/test/_spill3.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 1.228KiB   2023-01-09 15:05:06 qbe-1.1/test/abi1.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 413B       2023-01-09 15:05:06 qbe-1.1/test/abi2.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 870B       2023-01-09 15:05:06 qbe-1.1/test/abi3.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 612B       2023-01-09 15:05:06 qbe-1.1/test/abi4.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 3.414KiB   2023-01-09 15:05:06 qbe-1.1/test/abi5.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 845B       2023-01-09 15:05:06 qbe-1.1/test/abi6.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 361B       2023-01-09 15:05:06 qbe-1.1/test/abi7.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 7.185KiB   2023-01-09 15:05:06 qbe-1.1/test/abi8.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 248B       2023-01-09 15:05:06 qbe-1.1/test/align.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 244B       2023-01-09 15:05:06 qbe-1.1/test/cmp1.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 1.226KiB   2023-01-09 15:05:06 qbe-1.1/test/collatz.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 1.17KiB    2023-01-09 15:05:06 qbe-1.1/test/conaddr.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 1.854KiB   2023-01-09 15:05:06 qbe-1.1/test/cprime.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 269B       2023-01-09 15:05:06 qbe-1.1/test/cup.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 633B       2023-01-09 15:05:06 qbe-1.1/test/dark.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 407B       2023-01-09 15:05:06 qbe-1.1/test/double.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 339B       2023-01-09 15:05:06 qbe-1.1/test/dynalloc.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 574B       2023-01-09 15:05:06 qbe-1.1/test/echo.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 386B       2023-01-09 15:05:06 qbe-1.1/test/env.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 375B       2023-01-09 15:05:06 qbe-1.1/test/eucl.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 425B       2023-01-09 15:05:06 qbe-1.1/test/euclc.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 242B       2023-01-09 15:05:06 qbe-1.1/test/fixarg.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 682B       2023-01-09 15:05:06 qbe-1.1/test/fold1.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 2.407KiB   2023-01-09 15:05:06 qbe-1.1/test/fpcnv.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 535B       2023-01-09 15:05:06 qbe-1.1/test/isel1.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 2.364KiB   2023-01-09 15:05:06 qbe-1.1/test/isel2.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 1.649KiB   2023-01-09 15:05:06 qbe-1.1/test/isel3.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 637B       2023-01-09 15:05:06 qbe-1.1/test/ldbits.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 288B       2023-01-09 15:05:06 qbe-1.1/test/ldhoist.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 473B       2023-01-09 15:05:06 qbe-1.1/test/load1.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 1.314KiB   2023-01-09 15:05:06 qbe-1.1/test/load2.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 685B       2023-01-09 15:05:06 qbe-1.1/test/load3.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 357B       2023-01-09 15:05:06 qbe-1.1/test/loop.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 5.203KiB   2023-01-09 15:05:06 qbe-1.1/test/mandel.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 574B       2023-01-09 15:05:06 qbe-1.1/test/max.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 570B       2023-01-09 15:05:06 qbe-1.1/test/mem1.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 566B       2023-01-09 15:05:06 qbe-1.1/test/philv.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 552B       2023-01-09 15:05:06 qbe-1.1/test/prime.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 312B       2023-01-09 15:05:06 qbe-1.1/test/puts10.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 5.193KiB   2023-01-09 15:05:06 qbe-1.1/test/queen.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 457B       2023-01-09 15:05:06 qbe-1.1/test/rega1.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 1.376KiB   2023-01-09 15:05:06 qbe-1.1/test/spill1.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 1.146KiB   2023-01-09 15:05:06 qbe-1.1/test/strcmp.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 1.316KiB   2023-01-09 15:05:06 qbe-1.1/test/strspn.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 608B       2023-01-09 15:05:06 qbe-1.1/test/sum.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 543B       2023-01-09 15:05:06 qbe-1.1/test/tls.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 547B       2023-01-09 15:05:06 qbe-1.1/test/vararg1.ssa
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 21.23KiB   2023-01-09 15:05:06 qbe-1.1/test/vararg2.ssa
2025/01/11 16:36:30 dir   -rwxrwxr-x 0/0 (root/root) 0B         2023-01-09 15:05:06 qbe-1.1/tools/
2025/01/11 16:36:30 file  -rwxrwxr-x 0/0 (root/root) 2.866KiB   2023-01-09 15:05:06 qbe-1.1/tools/abi8.py
2025/01/11 16:36:30 file  -rwxrwxr-x 0/0 (root/root) 1.28KiB    2023-01-09 15:05:06 qbe-1.1/tools/abifuzz.sh
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 13.34KiB   2023-01-09 15:05:06 qbe-1.1/tools/callgen.ml
2025/01/11 16:36:30 file  -rwxrwxr-x 0/0 (root/root) 503B       2023-01-09 15:05:06 qbe-1.1/tools/cra.sh
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 1.926KiB   2023-01-09 15:05:06 qbe-1.1/tools/lexh.c
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 4.86KiB    2023-01-09 15:05:06 qbe-1.1/tools/pmov.c
2025/01/11 16:36:30 file  -rwxrwxr-x 0/0 (root/root) 2.943KiB   2023-01-09 15:05:06 qbe-1.1/tools/test.sh
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 3.071KiB   2023-01-09 15:05:06 qbe-1.1/tools/vatest.py
2025/01/11 16:36:30 file  -rw-rw-r-- 0/0 (root/root) 8.86KiB    2023-01-09 15:05:06 qbe-1.1/util.c
---------------------------------
Start:    2025-01-11 16:36:30.544678571 +0200 EET m=+0.004040708
End:      2025-01-11 16:36:30.676850244 +0200 EET m=+0.136212319
Duration: 132.171611ms

Counts (file/dir/link):  125 / 9 / 0
Total Bytes:             1.478MiB
Written Bytes:           117.2KiB
Deduped Bytes:           1.649MiB
Ratio (written / total): 7%
---------------------------------
Created archive with hash: 498a17762af7cd61428911b278c45b09ca93ead811bbc34241165ed65de02687
$ du -sh --apparent-size repo/
1.1M	repo/
</pre>
</details>

<details>
  <summary>qbe-1.2.tar</summary>
<pre>
$ ls -l qbe-1.2.tar
-rw-r--r-- 1 user user 1658880 Feb 16  2024 qbe-1.2.tar
$ niro tar import -v -r ./repo -i qbe-1.2.tar -n 'qbe-1.2.tar' -p '7szz31pqLTVay6W2lWZro7PzK23Tlv9JLoM1/7ZBdw8='
2025/01/11 16:38:06 file  ---------- 0/0 (/) 0B         0001-01-01 00:00:00 pax_global_header
2025/01/11 16:38:06 dir   -rwxrwxr-x 0/0 (root/root) 0B         2024-01-29 11:24:41 qbe-1.2/
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 32B        2024-01-29 11:24:41 qbe-1.2/.gitignore
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 1.05KiB    2024-01-29 11:24:41 qbe-1.2/LICENSE
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 2.394KiB   2024-01-29 11:24:41 qbe-1.2/Makefile
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 536B       2024-01-29 11:24:41 qbe-1.2/README
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 407B       2024-01-29 11:24:41 qbe-1.2/abi.c
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 4.253KiB   2024-01-29 11:24:41 qbe-1.2/alias.c
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 9.264KiB   2024-01-29 11:24:41 qbe-1.2/all.h
2025/01/11 16:38:06 dir   -rwxrwxr-x 0/0 (root/root) 0B         2024-01-29 11:24:41 qbe-1.2/amd64/
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 903B       2024-01-29 11:24:41 qbe-1.2/amd64/all.h
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 14.34KiB   2024-01-29 11:24:41 qbe-1.2/amd64/emit.c
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 16.1KiB    2024-01-29 11:24:41 qbe-1.2/amd64/isel.c
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 14.67KiB   2024-01-29 11:24:41 qbe-1.2/amd64/sysv.c
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 835B       2024-01-29 11:24:41 qbe-1.2/amd64/targ.c
2025/01/11 16:38:06 dir   -rwxrwxr-x 0/0 (root/root) 0B         2024-01-29 11:24:41 qbe-1.2/arm64/
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 16.3KiB    2024-01-29 11:24:41 qbe-1.2/arm64/abi.c
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 897B       2024-01-29 11:24:41 qbe-1.2/arm64/all.h
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 12.97KiB   2024-01-29 11:24:41 qbe-1.2/arm64/emit.c
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 5.551KiB   2024-01-29 11:24:41 qbe-1.2/arm64/isel.c
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 1.269KiB   2024-01-29 11:24:41 qbe-1.2/arm64/targ.c
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 4.966KiB   2024-01-29 11:24:41 qbe-1.2/cfg.c
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 4.403KiB   2024-01-29 11:24:41 qbe-1.2/copy.c
2025/01/11 16:38:06 dir   -rwxrwxr-x 0/0 (root/root) 0B         2024-01-29 11:24:41 qbe-1.2/doc/
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 4.972KiB   2024-01-29 11:24:41 qbe-1.2/doc/abi.txt
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 36.5KiB    2024-01-29 11:24:41 qbe-1.2/doc/il.txt
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 3.337KiB   2024-01-29 11:24:41 qbe-1.2/doc/llvm.txt
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 537B       2024-01-29 11:24:41 qbe-1.2/doc/rv64.txt
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 647B       2024-01-29 11:24:41 qbe-1.2/doc/win.txt
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 4.346KiB   2024-01-29 11:24:41 qbe-1.2/emit.c
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 11.07KiB   2024-01-29 11:24:41 qbe-1.2/fold.c
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 3.031KiB   2024-01-29 11:24:41 qbe-1.2/live.c
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 9.371KiB   2024-01-29 11:24:41 qbe-1.2/load.c
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 3.484KiB   2024-01-29 11:24:41 qbe-1.2/main.c
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 9.088KiB   2024-01-29 11:24:41 qbe-1.2/mem.c
2025/01/11 16:38:06 dir   -rwxrwxr-x 0/0 (root/root) 0B         2024-01-29 11:24:41 qbe-1.2/minic/
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 21B        2024-01-29 11:24:41 qbe-1.2/minic/.gitignore
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 146B       2024-01-29 11:24:41 qbe-1.2/minic/Makefile
2025/01/11 16:38:06 file  -rwxrwxr-x 0/0 (root/root) 470B       2024-01-29 11:24:41 qbe-1.2/minic/mcc
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 15.45KiB   2024-01-29 11:24:41 qbe-1.2/minic/minic.y
2025/01/11 16:38:06 dir   -rwxrwxr-x 0/0 (root/root) 0B         2024-01-29 11:24:41 qbe-1.2/minic/test/
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 401B       2024-01-29 11:24:41 qbe-1.2/minic/test/collatz.c
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 340B       2024-01-29 11:24:41 qbe-1.2/minic/test/euler9.c
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 776B       2024-01-29 11:24:41 qbe-1.2/minic/test/knight.c
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 1.311KiB   2024-01-29 11:24:41 qbe-1.2/minic/test/mandel.c
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 294B       2024-01-29 11:24:41 qbe-1.2/minic/test/prime.c
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 921B       2024-01-29 11:24:41 qbe-1.2/minic/test/queen.c
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 22.79KiB   2024-01-29 11:24:41 qbe-1.2/minic/yacc.c
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 7.661KiB   2024-01-29 11:24:41 qbe-1.2/ops.h
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 24.54KiB   2024-01-29 11:24:41 qbe-1.2/parse.c
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 13.9KiB    2024-01-29 11:24:41 qbe-1.2/rega.c
2025/01/11 16:38:06 dir   -rwxrwxr-x 0/0 (root/root) 0B         2024-01-29 11:24:41 qbe-1.2/rv64/
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 12.58KiB   2024-01-29 11:24:41 qbe-1.2/rv64/abi.c
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 994B       2024-01-29 11:24:41 qbe-1.2/rv64/all.h
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 11.84KiB   2024-01-29 11:24:41 qbe-1.2/rv64/emit.c
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 5.431KiB   2024-01-29 11:24:41 qbe-1.2/rv64/isel.c
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 1.084KiB   2024-01-29 11:24:41 qbe-1.2/rv64/targ.c
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 1.429KiB   2024-01-29 11:24:41 qbe-1.2/simpl.c
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 10.42KiB   2024-01-29 11:24:41 qbe-1.2/spill.c
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 7.681KiB   2024-01-29 11:24:41 qbe-1.2/ssa.c
2025/01/11 16:38:06 dir   -rwxrwxr-x 0/0 (root/root) 0B         2024-01-29 11:24:41 qbe-1.2/test/
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 529B       2024-01-29 11:24:41 qbe-1.2/test/_alt.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 51.94KiB   2024-01-29 11:24:41 qbe-1.2/test/_bf99.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 154KiB     2024-01-29 11:24:41 qbe-1.2/test/_bfmandel.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 5.425KiB   2024-01-29 11:24:41 qbe-1.2/test/_chacha20.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 403B       2024-01-29 11:24:41 qbe-1.2/test/_dragon.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 155B       2024-01-29 11:24:41 qbe-1.2/test/_fix1.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 166B       2024-01-29 11:24:41 qbe-1.2/test/_fix2.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 225B       2024-01-29 11:24:41 qbe-1.2/test/_fix3.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 353B       2024-01-29 11:24:41 qbe-1.2/test/_fix4.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 351B       2024-01-29 11:24:41 qbe-1.2/test/_live.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 111B       2024-01-29 11:24:41 qbe-1.2/test/_rpo.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 851.4KiB   2024-01-29 11:24:41 qbe-1.2/test/_slow.qbe
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 335B       2024-01-29 11:24:41 qbe-1.2/test/_spill1.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 410B       2024-01-29 11:24:41 qbe-1.2/test/_spill2.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 368B       2024-01-29 11:24:41 qbe-1.2/test/_spill3.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 1.228KiB   2024-01-29 11:24:41 qbe-1.2/test/abi1.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 413B       2024-01-29 11:24:41 qbe-1.2/test/abi2.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 870B       2024-01-29 11:24:41 qbe-1.2/test/abi3.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 612B       2024-01-29 11:24:41 qbe-1.2/test/abi4.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 3.414KiB   2024-01-29 11:24:41 qbe-1.2/test/abi5.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 845B       2024-01-29 11:24:41 qbe-1.2/test/abi6.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 361B       2024-01-29 11:24:41 qbe-1.2/test/abi7.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 7.185KiB   2024-01-29 11:24:41 qbe-1.2/test/abi8.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 248B       2024-01-29 11:24:41 qbe-1.2/test/align.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 244B       2024-01-29 11:24:41 qbe-1.2/test/cmp1.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 1.226KiB   2024-01-29 11:24:41 qbe-1.2/test/collatz.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 1.17KiB    2024-01-29 11:24:41 qbe-1.2/test/conaddr.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 1.854KiB   2024-01-29 11:24:41 qbe-1.2/test/cprime.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 269B       2024-01-29 11:24:41 qbe-1.2/test/cup.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 633B       2024-01-29 11:24:41 qbe-1.2/test/dark.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 407B       2024-01-29 11:24:41 qbe-1.2/test/double.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 339B       2024-01-29 11:24:41 qbe-1.2/test/dynalloc.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 574B       2024-01-29 11:24:41 qbe-1.2/test/echo.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 386B       2024-01-29 11:24:41 qbe-1.2/test/env.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 375B       2024-01-29 11:24:41 qbe-1.2/test/eucl.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 425B       2024-01-29 11:24:41 qbe-1.2/test/euclc.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 242B       2024-01-29 11:24:41 qbe-1.2/test/fixarg.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 682B       2024-01-29 11:24:41 qbe-1.2/test/fold1.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 2.421KiB   2024-01-29 11:24:41 qbe-1.2/test/fpcnv.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 535B       2024-01-29 11:24:41 qbe-1.2/test/isel1.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 2.364KiB   2024-01-29 11:24:41 qbe-1.2/test/isel2.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 1.649KiB   2024-01-29 11:24:41 qbe-1.2/test/isel3.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 637B       2024-01-29 11:24:41 qbe-1.2/test/ldbits.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 288B       2024-01-29 11:24:41 qbe-1.2/test/ldhoist.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 473B       2024-01-29 11:24:41 qbe-1.2/test/load1.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 1.314KiB   2024-01-29 11:24:41 qbe-1.2/test/load2.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 685B       2024-01-29 11:24:41 qbe-1.2/test/load3.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 357B       2024-01-29 11:24:41 qbe-1.2/test/loop.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 5.203KiB   2024-01-29 11:24:41 qbe-1.2/test/mandel.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 574B       2024-01-29 11:24:41 qbe-1.2/test/max.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 570B       2024-01-29 11:24:41 qbe-1.2/test/mem1.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 581B       2024-01-29 11:24:41 qbe-1.2/test/mem2.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 1.021KiB   2024-01-29 11:24:41 qbe-1.2/test/mem3.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 566B       2024-01-29 11:24:41 qbe-1.2/test/philv.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 552B       2024-01-29 11:24:41 qbe-1.2/test/prime.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 312B       2024-01-29 11:24:41 qbe-1.2/test/puts10.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 5.193KiB   2024-01-29 11:24:41 qbe-1.2/test/queen.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 457B       2024-01-29 11:24:41 qbe-1.2/test/rega1.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 1.376KiB   2024-01-29 11:24:41 qbe-1.2/test/spill1.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 1.146KiB   2024-01-29 11:24:41 qbe-1.2/test/strcmp.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 1.316KiB   2024-01-29 11:24:41 qbe-1.2/test/strspn.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 608B       2024-01-29 11:24:41 qbe-1.2/test/sum.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 1.264KiB   2024-01-29 11:24:41 qbe-1.2/test/tls.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 547B       2024-01-29 11:24:41 qbe-1.2/test/vararg1.ssa
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 21.23KiB   2024-01-29 11:24:41 qbe-1.2/test/vararg2.ssa
2025/01/11 16:38:06 dir   -rwxrwxr-x 0/0 (root/root) 0B         2024-01-29 11:24:41 qbe-1.2/tools/
2025/01/11 16:38:06 file  -rwxrwxr-x 0/0 (root/root) 2.866KiB   2024-01-29 11:24:41 qbe-1.2/tools/abi8.py
2025/01/11 16:38:06 file  -rwxrwxr-x 0/0 (root/root) 1.28KiB    2024-01-29 11:24:41 qbe-1.2/tools/abifuzz.sh
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 13.34KiB   2024-01-29 11:24:41 qbe-1.2/tools/callgen.ml
2025/01/11 16:38:06 file  -rwxrwxr-x 0/0 (root/root) 503B       2024-01-29 11:24:41 qbe-1.2/tools/cra.sh
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 1.946KiB   2024-01-29 11:24:41 qbe-1.2/tools/lexh.c
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 4.86KiB    2024-01-29 11:24:41 qbe-1.2/tools/pmov.c
2025/01/11 16:38:06 file  -rwxrwxr-x 0/0 (root/root) 2.874KiB   2024-01-29 11:24:41 qbe-1.2/tools/test.sh
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 3.071KiB   2024-01-29 11:24:41 qbe-1.2/tools/vatest.py
2025/01/11 16:38:06 file  -rw-rw-r-- 0/0 (root/root) 8.98KiB    2024-01-29 11:24:41 qbe-1.2/util.c
---------------------------------
Start:    2025-01-11 16:38:06.066599475 +0200 EET m=+0.003640701
End:      2025-01-11 16:38:06.180056185 +0200 EET m=+0.117097072
Duration: 113.456371ms

Counts (file/dir/link):  127 / 9 / 0
Total Bytes:             1.485MiB
Written Bytes:           83.35KiB
Deduped Bytes:           1.746MiB
Ratio (written / total): 5%
---------------------------------
Created archive with hash: 5ce32684e99681bdf892e574ca37c0aa0aac4696d5288a7be00fcc8b0b5b81ed
$ du -sh --apparent-size repo/
1.2M	repo/
</pre>
</details>


## Mount
Uses FUSE to mount Niro archive to the filesystem.

```
$ niro mount -r ./repo/ -H '5ce32684e99681bdf892e574ca37c0aa0aac4696d5288a7be00fcc8b0b5b81ed' -s 'KEXr3kP/GY0pksw/51nppYDB1c2qtUhpoiWhzgZ2Wos=' ./mount/
2025/01/11 16:52:56 FUSE running...
```

```
user@host:mount$ ls -ashl
total 0
0 -rw-r--r-- 0 root root 0 Jan  1  0001 pax_global_header
0 drwxr-xr-x 0 root root 0 Jan  1  1970 qbe-1.2
user@host:~/niro/mount/qbe-1.2$ ls -ashl qbe-1.2/
total 196K
4.0K -rw-rw-r-- 0 root root  407 Jan 29  2024 abi.c
8.0K -rw-rw-r-- 0 root root 4.3K Jan 29  2024 alias.c
 12K -rw-rw-r-- 0 root root 9.3K Jan 29  2024 all.h
   0 drwxr-xr-x 0 root root    0 Jan  1  1970 amd64
   0 drwxr-xr-x 0 root root    0 Jan  1  1970 arm64
8.0K -rw-rw-r-- 0 root root 5.0K Jan 29  2024 cfg.c
8.0K -rw-rw-r-- 0 root root 4.5K Jan 29  2024 copy.c
   0 drwxr-xr-x 0 root root    0 Jan  1  1970 doc
8.0K -rw-rw-r-- 0 root root 4.4K Jan 29  2024 emit.c
 12K -rw-rw-r-- 0 root root  12K Jan 29  2024 fold.c
4.0K -rw-rw-r-- 0 root root   32 Jan 29  2024 .gitignore
4.0K -rw-rw-r-- 0 root root 1.1K Jan 29  2024 LICENSE
4.0K -rw-rw-r-- 0 root root 3.1K Jan 29  2024 live.c
 12K -rw-rw-r-- 0 root root 9.4K Jan 29  2024 load.c
4.0K -rw-rw-r-- 0 root root 3.5K Jan 29  2024 main.c
4.0K -rw-rw-r-- 0 root root 2.4K Jan 29  2024 Makefile
 12K -rw-rw-r-- 0 root root 9.1K Jan 29  2024 mem.c
   0 drwxr-xr-x 0 root root    0 Jan  1  1970 minic
8.0K -rw-rw-r-- 0 root root 7.7K Jan 29  2024 ops.h
 28K -rw-rw-r-- 0 root root  25K Jan 29  2024 parse.c
4.0K -rw-rw-r-- 0 root root  536 Jan 29  2024 README
 16K -rw-rw-r-- 0 root root  14K Jan 29  2024 rega.c
   0 drwxr-xr-x 0 root root    0 Jan  1  1970 rv64
4.0K -rw-rw-r-- 0 root root 1.5K Jan 29  2024 simpl.c
 12K -rw-rw-r-- 0 root root  11K Jan 29  2024 spill.c
8.0K -rw-rw-r-- 0 root root 7.7K Jan 29  2024 ssa.c
   0 drwxr-xr-x 0 root root    0 Jan  1  1970 test
   0 drwxr-xr-x 0 root root    0 Jan  1  1970 tools
 12K -rw-rw-r-- 0 root root 9.0K Jan 29  2024 util.c
```
