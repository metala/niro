package niro

import (
	"context"
	"fmt"
	"io"
	"runtime"
	"sync"

	"go.metala.org/niro/concurrency"
	"go.metala.org/niro/tnk"
)

type WriteResult struct {
	Hash      tnk.Hash
	HashFlags tnk.HashFlags
	Total     uint64
	Written   uint64
	Deduped   uint64
}

func (n *Niro) WriteFile(f io.Reader) (*WriteResult, error) {
	chunks, err := n.writeChunks(f)
	if err != nil {
		return nil, fmt.Errorf("write chunks: %w", err)
	}
	if len(chunks) == 1 {
		deduped := uint64(0)
		if chunks[0].Exists {
			deduped = uint64(chunks[0].Size)
		}
		return &WriteResult{
			Hash:      chunks[0].Hash,
			HashFlags: 0,
			Total:     uint64(chunks[0].Size),
			Written:   uint64(chunks[0].Written),
			Deduped:   deduped,
		}, nil
	}
	result, err := n.tnk.WriteComposite(n.publicKey, chunks)
	if err != nil {
		return nil, fmt.Errorf("write composite: %w", err)
	}
	total := result.Size
	written := result.Written
	deduped := uint64(0)
	if result.Exists {
		deduped = result.Size
	}
	for _, chunk := range chunks {
		total += uint64(chunk.Size)
		written += uint64(chunk.Written)
		if chunk.Exists {
			deduped += uint64(chunks[0].Size)
		}
	}
	if n.Verbosity >= 2 {
		logHash("total", result.Hash, total, written, result.Exists)
	}
	return &WriteResult{
		Hash:      result.Hash,
		HashFlags: tnk.FlagComposite,
		Total:     total,
		Written:   written,
		Deduped:   deduped,
	}, nil
}

func (n *Niro) WriteFileAsync(f io.Reader) (<-chan *WriteResult, <-chan error) {
	errc := make(chan error, 1)
	resultc := make(chan *WriteResult, 1)
	go func() {
		defer close(errc)
		defer close(resultc)
		result, err := n.WriteFile(f)
		if err != nil {
			errc <- err
		} else {
			resultc <- result
		}
	}()
	return resultc, errc
}

func (n *Niro) ReadFile(hash tnk.Hash, composite bool) (io.ReadSeekCloser, error) {
	r, err := n.tnk.OpenReader(hash, n.secretKey)
	if !composite {
		return r, err
	}
	defer r.Close()
	return tnk.NewCompositeReader(n.tnk, n.secretKey, r)
}

func (n *Niro) writeChunks(r io.Reader) ([]tnk.Chunk, error) {
	var results []tnk.Chunk

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	chunkChan, errc := n.tnk.ChunkAsync(ctx, r)
	if n.Concurrency == 1 {
		for data := range chunkChan {
			size := uint32(len(data))
			result, err := n.WriteObjectData(tnk.DataType, data)
			if err != nil {
				return nil, fmt.Errorf("write object: %w", err)
			}
			results = append(results, tnk.Chunk{
				Size:    size,
				Written: uint32(result.Written),
				Hash:    result.Hash,
				Exists:  result.Exists,
			})
		}
		if err := <-errc; err != nil {
			return nil, err
		}
		return results, nil
	}

	type resultType struct {
		result tnk.Chunk
		err    error
	}
	numWorkers := int(n.Concurrency)
	if numWorkers == 0 {
		numWorkers = runtime.NumCPU()
	}
	processor := func(data []byte) resultType {
		size := uint32(len(data))
		result, err := n.WriteObjectData(tnk.DataType, data)
		if err != nil {
			return resultType{err: err}
		}
		return resultType{
			result: tnk.Chunk{
				Size:    size,
				Written: uint32(result.Written),
				Hash:    result.Hash,
				Exists:  result.Exists,
			},
		}
	}
	workers := concurrency.NewWorkerGroup(ctx, processor, int(numWorkers))
	jobs := &sync.WaitGroup{}
	go func() {
		for data := range chunkChan {
			select {
			case <-ctx.Done():
				workers.Stop()
				return
			default:
				jobs.Add(1)
				workers.AddWork(data)
			}
		}
		jobs.Wait()
		workers.Stop()
	}()
	for result := range workers.Output() {
		if result.err != nil {
			cancel()
			return nil, fmt.Errorf("worker write object: %w", result.err)
		}
		if ctx.Err() != nil {
			break
		}
		results = append(results, result.result)
		jobs.Done()
	}
	if err := <-errc; err != nil {
		return nil, err
	}
	return results, nil
}
