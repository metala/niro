package tnk

import (
	"bytes"
	"fmt"
	"io"
)

type ObjectType uint8

const (
	DataType ObjectType = iota
	CompositeV0Type
	CompositeType
	TreeType
	ArchiveType
)

var ZeroHash = [32]byte{}

func (t *Tnk) WriteObject(type_ ObjectType, pk *PublicKey, rd io.Reader) (*StoreResult, error) {
	if magic, ok := MagicMap[type_]; ok {
		rd = io.MultiReader(bytes.NewReader(magic), rd)
	}
	result, err := t.Store(rd, pk)
	if err != nil {
		return nil, fmt.Errorf("tnk.Store: %w", err)
	}
	return result, nil
}
