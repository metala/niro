package tnk

import (
	"context"
	"io"

	"github.com/jotfs/fastcdc-go"
)

func (c *Tnk) ChunkAsync(ctx context.Context, r io.Reader) (<-chan []byte, <-chan error) {
	errc := make(chan error, 1)
	datac := make(chan []byte, 1)

	chunker, _ := fastcdc.NewChunker(r, c.chunkOpts)
	go func() {
		defer close(errc)
		defer close(datac)
		for {
			if err := ctx.Err(); err != nil {
				errc <- err
				return
			}
			chunk, err := chunker.Next()
			if chunk.Length > 0 {
				data := make([]byte, len(chunk.Data))
				copy(data, chunk.Data)
				datac <- data
			}
			if err == io.EOF {
				break
			} else if err != nil {
				errc <- err
				return
			}
		}
	}()

	return datac, errc
}
