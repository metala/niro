package main

import (
	"encoding/hex"
	"fmt"
	"log"
	"os"
	"os/signal"
	"runtime"
	"time"

	"github.com/hanwen/go-fuse/v2/fs"
	"github.com/hanwen/go-fuse/v2/fuse"
	"github.com/spf13/afero"
	"github.com/spf13/cobra"
	"go.metala.org/niro"
	"go.metala.org/niro/contrib/fusefs"
)

var (
	allowOther bool
)

func init() {
	mountCmd.Flags().StringVarP(&repositoryPath, "repository", "r", "", `/path/to/repository`)
	mountCmd.MarkFlagRequired("repository")
	mountCmd.Flags().BytesBase64VarP(&secretKey, "secret-key", "s", nil, `secret key in base64 format`)
	mountCmd.MarkFlagRequired("secret-key")
	mountCmd.Flags().StringVarP(&hash, "hash", "H", "", `archive hash`)
	mountCmd.MarkFlagRequired("hash")
	mountCmd.Flags().BoolVarP(&allowOther, "allow-other", "", false, `allow other users to access the mount`)
	rootCmd.AddCommand(mountCmd)
}

var mountCmd = &cobra.Command{
	Use:   "mount",
	Short: "Mount archive at filesystem",
	Long:  `Uses FUSE to mount Niro archive to the filesystem.`,
	Args:  cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		repoFs := afero.NewBasePathFs(afero.NewOsFs(), repositoryPath)
		repo, err := niro.Existing(repoFs)
		if err != nil {
			fmt.Fprintf(os.Stderr, "failed to open repository: %s\n", err)
			os.Exit(2)
		}
		repo.Verbosity = uint(verbosity)
		if err = repo.SetSecretKey(secretKey); err != nil {
			fmt.Fprintf(os.Stderr, "invalid secret key: %s\n", err)
			os.Exit(3)
		}
		h, err := hex.DecodeString(hash)
		if err != nil {
			fmt.Fprintf(os.Stderr, "invalid hash: %s\n", err)
			os.Exit(2)
		}

		ttl := 60 * time.Second
		mountPoint := args[0]
		root, err := fusefs.NewArchiveTree(repo, h)
		if err != nil {
			fmt.Printf("Load archive root fail: %v\n", err)
			os.Exit(1)
		}
		opts := &fs.Options{
			AttrTimeout:  &ttl,
			EntryTimeout: &ttl,
			MountOptions: fuse.MountOptions{
				FsName:     fmt.Sprintf("%s#%s", repositoryPath, hash),
				Name:       "nirofs",
				SyncRead:   true,
				AllowOther: allowOther,
			},
		}
		opts.Debug = verbosity >= 2
		server, err := fs.Mount(mountPoint, root, opts)
		if err != nil {
			fmt.Printf("Mount fail: %v\n", err)
			os.Exit(1)
		}

		runtime.GC()
		log.Print("FUSE running...")
		done := make(chan struct{})
		go func() {
			server.Wait()
			close(done)
		}()

		interrupt := make(chan os.Signal, 1)
		signal.Notify(interrupt, os.Interrupt)
		select {
		case <-interrupt:
			log.Print("Interrupted. Unmounting...")
			server.Unmount()
		case <-done:
			log.Print("Exited gracefully.")
		}
	},
}
