rwildcard=$(foreach d,$(wildcard $(1:=/*)),$(call rwildcard,$d,$2) $(filter $(subst *,%,$2),$d))

.PHONY: all

bin/niro: Makefile $(call rwildcard,.,*.go)
	CGO_ENABLED=0 go build -ldflags="-extldflags=-static" -o bin/niro ./cli/

install: bin/niro
	cp bin/niro ~/bin/niro

all: bin/niro bin/niro-static
